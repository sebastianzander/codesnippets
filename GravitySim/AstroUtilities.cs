using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public static class AstroUtilities
{
    private const int ANOMALY_MAX_ITERATIONS = 30;
    private const int ANOMALY_PRECISION = 10;

    public const float hrs2deg = 15f;
    public const float deg2hrs = 1 / 15f;

    public static double CalculateEccentricAnomaly(double meanAnomalyDeg, double eccentricity)
    {
        int i = 0;
        double precision = Mathf.Pow(10, -ANOMALY_PRECISION);
        double meanAnomalyRad = 2 * Math.PI * (meanAnomalyDeg / 360 - Math.Floor(meanAnomalyDeg / 360));
        double eccentricAnomalyRad = eccentricity < 0.8f ? meanAnomalyRad : Mathf.PI;
        double deltaEccentricAnomalyRad = eccentricAnomalyRad - eccentricity * Math.Sin(meanAnomalyRad) - meanAnomalyRad;
        while(Math.Abs(deltaEccentricAnomalyRad) > precision && i < ANOMALY_MAX_ITERATIONS)
        {
            eccentricAnomalyRad -= deltaEccentricAnomalyRad / (1f - eccentricity * Math.Cos(eccentricAnomalyRad));
            deltaEccentricAnomalyRad = eccentricAnomalyRad - eccentricity * Math.Sin(eccentricAnomalyRad) - meanAnomalyRad;
            i += 1;
        }
        return MathUtilities.NormalizeAngle(eccentricAnomalyRad * Mathf.Rad2Deg);
    }

    public static double CalculateTrueAnomaly(double eccentricAnomalyDeg, double eccentricity)
    {
        double eccentricAnomalyRad = eccentricAnomalyDeg * Mathf.Deg2Rad;
        double trueAnomalyRad = Math.Atan2(Math.Sqrt(1 - eccentricity * eccentricity) * Math.Sin(eccentricAnomalyRad), Math.Cos(eccentricAnomalyRad) - eccentricity);
        return MathUtilities.NormalizeAngle(trueAnomalyRad * Mathf.Rad2Deg);
    }

    public static Vector3 GetVernalEquinoxDirection(Body body)
    {
        if(body == null)
            return Vector3.zero;

        Vector3 orbitalPlaneNormal = body.GetOrbitalPlaneNormal();
        Vector3 equatorialPlaneNormal = body.transform.up;

        return GetVernalEquinoxDirection(equatorialPlaneNormal, orbitalPlaneNormal);
    }

    public static Vector3 GetVernalEquinoxDirection(Vector3 equatorialPlaneNormal, Vector3 orbitalPlaneNormal)
    {
        if(equatorialPlaneNormal == Vector3.zero || orbitalPlaneNormal == Vector3.zero)
            return Vector3.zero;

        return Vector3.Cross(orbitalPlaneNormal, equatorialPlaneNormal);
    }

    public static Vector3 GetLookDirectionAtLocation(Transform location, Body holdingBody, EquatorialCoords coords)
    {
        if(location == null || holdingBody == null || coords == null)
            return Vector3.zero;

        Vector3 vernalEquinox = GetVernalEquinoxDirection(holdingBody);
        return GetLookDirectionAtLocation(location, vernalEquinox, coords);
    }

    public static Vector3 GetLookDirectionAtLocation(Transform location, Vector3 vernalEquinox, EquatorialCoords coords)
    {
        if(location == null || vernalEquinox == Vector3.zero || coords == null)
            return Vector3.zero;

        Vector3 lookDirection = new Vector3();
        return lookDirection;
    }

    public static int GetVisibilityAtLocation(Transform location, Body holdingBody, EquatorialCoords coords)
    {
        if(location == null || holdingBody == null || coords == null)
            return -1;

        Vector3 lookDirection = GetLookDirectionAtLocation(location, holdingBody, coords);
        if(lookDirection == Vector3.zero)
            return -2;

        // point at coords is below the horizon (simplified model)
        float dot = Vector3.Dot(lookDirection, location.up);
        if(dot < 0f)
            return 0;

        // TODO: check for general visibility any given latitude (longitude remains fixed) - accounts for axial rotation

        // currently returns only momentary visibility
        if(dot >= 0f)
            return 1;

        int visibility = 0;
        return visibility;
    }

    //public static EquatorialCoords GetEquatorialCoords(Body origin, Body target, double julianDate)
    //{
    //    Vector3 vernalEquinox = GetVernalEquinoxDirection(origin);
    //    if(vernalEquinox.Equals(Vector3.zero))
    //        return EquatorialCoords.undefined;
    //}

    public static double GetGreenwichMeanSiderealTime(double julianDate, bool hoursOnly = true)
    {
        // determine Greenwich Mean Sidereal Time (GMST)
        double julianDaysSinceJ2000 = julianDate - Engine.JULIAN_DATE_AT_J2000;
        double greenwichMeanSiderealTimeInHours = 18.697374558 + 24.06570982441908 * julianDaysSinceJ2000;
        if(hoursOnly)
            greenwichMeanSiderealTimeInHours = MathUtilities.Normalize(greenwichMeanSiderealTimeInHours, 24.0);
        return greenwichMeanSiderealTimeInHours;
    }

    public static double GetGreenwichApproximateSiderealTime(double julianDate, bool hoursOnly = true)
    {
        // determine Greenwich Approximate Sidereal Time (GAST)
        double greenwichMeanSiderealTimeInHours = GetGreenwichMeanSiderealTime(julianDate, hoursOnly);
        double equationOfEquinoxes = GetEquationOfEquinoxes(greenwichMeanSiderealTimeInHours);
        double greenwichApproximateSiderealTimeInHours = greenwichMeanSiderealTimeInHours + equationOfEquinoxes;
        if(hoursOnly)
            greenwichApproximateSiderealTimeInHours = MathUtilities.Normalize(greenwichApproximateSiderealTimeInHours, 24.0);
        return greenwichApproximateSiderealTimeInHours;
    }

    public static double GetEquationOfEquinoxes(double greenwichMeanSiderealTimeInHours)
    {
        // calculate nutation and equation of equinoxes
        double longitudeOfAscendingNodeMoon = 125.04 - 0.052954 * greenwichMeanSiderealTimeInHours;
        double meanLongitudeOfSun = 280.47 + 0.98565 * greenwichMeanSiderealTimeInHours;
        double nutationEarth = (-0.000319 * MathUtilities.Dsin(longitudeOfAscendingNodeMoon) -
            0.000024 * MathUtilities.Dsin(2.0 * meanLongitudeOfSun));
        double obliquityEarth = 23.4393 - 0.0000004 * greenwichMeanSiderealTimeInHours;
        double equationOfEquinoxes = nutationEarth * MathUtilities.Dcos(obliquityEarth);
        return equationOfEquinoxes;
    }

    public static double GetLocalSiderealTime(double julianDate, float longitude, bool hoursOnly = true)
    {
        double greenwichApproximateSiderealTimeInHours = GetGreenwichApproximateSiderealTime(julianDate, hoursOnly);
        double localSiderealTimeInHours = greenwichApproximateSiderealTimeInHours + longitude * deg2hrs;
        if(hoursOnly)
            MathUtilities.Normalize(localSiderealTimeInHours, 24.0);
        return localSiderealTimeInHours;
    }
}

public class HorizontalCoords
{
    [Unit(UnitType.deg)]
    private double _alt;
    [Unit(UnitType.deg)]
    private double _az;

    public static HorizontalCoords undefined = new HorizontalCoords() {
        _alt = Double.NegativeInfinity, _az = Double.NegativeInfinity
    };

    [Unit(UnitType.deg)]
    public double Altitude {
        get { return _alt; }
        set { _alt = MathUtilities.ClampAngle(value, -90.0, 90.0); }
    }

    [Unit(UnitType.deg)]
    public double Azimuth {
        get { return _az; }
        set { _az = MathUtilities.NormalizeAngle(value); }
    }

    public HorizontalCoords()
    {
        _alt = 0.0;
        _az = 0.0;
    }

    public HorizontalCoords(string coordString)
    {
        HorizontalCoords hc = ToCoords(coordString);
        _alt = hc._alt;
        _az = hc._az;
    }

    public HorizontalCoords(double alt, double az)
    {
        _alt = alt;
        _az = az;
    }

    public HorizontalCoords(HorizontalCoords horizontalCoords)
    {
        _alt = horizontalCoords._alt;
        _az = horizontalCoords._az;
    }

    public static HorizontalCoords ToCoords(string coordString)
    {
        double alt = 0.0;
        double az = 0.0;

        MatchCollection mc;

        string pattern1 = "([-0-9]+(.[0-9]+)?)°?,([ ]|[\t])*([-0-9]+(.[0-9]+)?)°?";
        mc = Regex.Matches(coordString, pattern1);

        if(mc.Count > 0)
        {
            Match m = mc[0];

            alt = float.Parse(m.Groups[1].Value);
            az = float.Parse(m.Groups[4].Value);
        }

        return new HorizontalCoords(alt, az);
    }

    public static HorizontalCoords ToCoords(EquatorialCoords equatorialCoords, double julianDate, float latitude, 
        float longitude, bool respectRefraction = false)
    {
        return EquatorialCoords.ToHorizontalCoords(equatorialCoords, julianDate, latitude, longitude, respectRefraction);
    }

    public static HorizontalCoords ToCoords(Transform horizontalCoordinateSystem, Transform target)
    {
        if(horizontalCoordinateSystem == null || target == null)
            return null;

        double alt = 0.0;
        double az = 0.0;

        Vector3 targetLocalPosition = horizontalCoordinateSystem.InverseTransformPoint(target.position);
        Vector3 targetLocalDirection = targetLocalPosition.normalized;

        alt = MathUtilities.NormalizeAltitude(90.0 - MathUtilities.Datan(Math.Sqrt(targetLocalDirection.x * targetLocalDirection.x +
            targetLocalDirection.z * targetLocalDirection.z) / targetLocalDirection.y));

        Vector2 targetAzimuthDirection = new Vector2(targetLocalDirection.z, targetLocalDirection.x);
        targetAzimuthDirection = targetAzimuthDirection.normalized;

        if(targetAzimuthDirection.x != 0.0)
            az = MathUtilities.Datan((double)targetAzimuthDirection.y / (double)targetAzimuthDirection.x);
        else
            az = 90.0 * Mathf.Sign(targetAzimuthDirection.y);

        if(targetAzimuthDirection.x < 0.0)
            az += 180.0;

        az = MathUtilities.NormalizeAngle(az + 90.0);

        return new HorizontalCoords(alt, az);
    }

    public static EquatorialCoords ToEquatorialCoords(HorizontalCoords horizontalCoords, double julianDate, float latitude, 
        float longitude, bool respectRefraction = false)
    {
        double localHourAngleInDegrees = 0f;
        double rightAscension = 0f;
        double declination = 0f;

        double localSiderealTimeInHours = AstroUtilities.GetLocalSiderealTime(julianDate, longitude);

        if(respectRefraction)
        {
            // not yet implemented
        }

        declination = MathUtilities.Dasin(MathUtilities.Dsin(horizontalCoords.Altitude) * MathUtilities.Dsin(latitude) + 
            MathUtilities.Dcos(horizontalCoords.Altitude) * MathUtilities.Dcos(latitude) * MathUtilities.Dcos(horizontalCoords.Azimuth));

        localHourAngleInDegrees = MathUtilities.NormalizeAngle(MathUtilities.Datan2(MathUtilities.Dsin(horizontalCoords.Azimuth),
            MathUtilities.Dsin(latitude) * MathUtilities.Dcos(horizontalCoords.Azimuth) - MathUtilities.Dcos(latitude) *
            MathUtilities.Dtan(horizontalCoords.Altitude)) + 180f);

        rightAscension = MathUtilities.NormalizeAngle(localSiderealTimeInHours * AstroUtilities.hrs2deg - localHourAngleInDegrees);

        return new EquatorialCoords((float)rightAscension, (float)declination);
    }

    public EquatorialCoords ToEquatorialCoords(double julianDate, float latitude, float longitude, bool respectRefraction = false)
    {
        return ToEquatorialCoords(this, julianDate, latitude, longitude, respectRefraction);
    }
}

public class EquatorialCoords
{
    [Unit(UnitType.deg)]
    private double _ra;
    [Unit(UnitType.deg)]
    private double _dec;

    [Unit(UnitType.hour_angle)]
    private int _ra_h;
    [Unit(UnitType.min_angle)]
    private int _ra_m;
    [Unit(UnitType.sec_angle)]
    private double _ra_s;

    [Unit(UnitType.deg)]
    private int _dec_d;
    [Unit(UnitType.arcmin)]
    private int _dec_m;
    [Unit(UnitType.arcsec)]
    private double _dec_s;
    private int _dec_sign;

    public static EquatorialCoords undefined = new EquatorialCoords() {
        _ra = Double.NegativeInfinity, _dec = Double.NegativeInfinity
    };

    [Unit(UnitType.deg)]
    public double RightAscension {
        get { return _ra; }
        set { _ra = MathUtilities.NormalizeAngle(value); UpdateRAHoursMinutesSeconds(); }
    }

    [Unit(UnitType.hour_angle)]
    public double RightAscensionInHours {
        get { return _ra * AstroUtilities.deg2hrs; }
        set { _ra = MathUtilities.NormalizeAngle(value * AstroUtilities.hrs2deg); UpdateRAHoursMinutesSeconds(); }
    }

    [Unit(UnitType.hour_angle)]
    public int RightAscensionHours {
        get { return _ra_h; }
        set { _ra_h = Mathf.Clamp(value, 0, 23); UpdateRADecimal(); }
    }

    [Unit(UnitType.min_angle)]
    public int RightAscensionMinutes {
        get { return _ra_m; }
        set { _ra_m = Mathf.Clamp(value, 0, 59); UpdateRADecimal(); }
    }

    [Unit(UnitType.sec_angle)]
    public double RightAscensionSeconds {
        get { return _ra_s; }
        set { _ra_s = MathUtilities.Clamp(value, 0.0, 59.999999); UpdateRADecimal(); }
    }

    [Unit(UnitType.deg)]
    public double Declination {
        get { return _dec; }
        set { _dec = MathUtilities.ClampAngle(value, -90.0, 90.0); UpdateDECDegreesMinutesSeconds(); }
    }

    [Unit(UnitType.deg)]
    public int DeclinationDegrees {
        get { return _dec_d; }
        set { _dec_d = Mathf.Clamp(value, -90, 90); UpdateDECDecimal(); }
    }

    [Unit(UnitType.arcmin)]
    public int DeclinationMinutes {
        get { return _dec_m; }
        set { _dec_m = Mathf.Clamp(value, 0, 59); UpdateDECDecimal(); }
    }

    [Unit(UnitType.arcsec)]
    public double DeclinationSeconds {
        get { return _dec_s; }
        set { _dec_s = MathUtilities.Clamp(value, 0.0, 59.999999); UpdateDECDecimal(); }
    }

    public EquatorialCoords(string coordString)
    {
        EquatorialCoords ec = EquatorialCoords.ToCoords(coordString);
        _ra = ec._ra;
        _dec = ec._dec;
    }

    public EquatorialCoords(double ra, double dec)
    {
        _ra = MathUtilities.NormalizeAngle(ra);
        _dec = MathUtilities.ClampAngle(dec, -90.0, 90.0);

        UpdateRAHoursMinutesSeconds();
        UpdateDECDegreesMinutesSeconds();
    }

    public EquatorialCoords(int ra_h, int ra_m, double ra_s, int dec_d, int dec_m, double dec_s)
    {
        _ra_h = MathUtilities.Clamp(ra_h, 0, 23);
        _ra_m = MathUtilities.Clamp(ra_m, 0, 59);
        _ra_s = MathUtilities.Clamp(ra_s, 0.0, 59.999999);

        _dec_d = (int)MathUtilities.NormalizeAngle(dec_d);
        _dec_m = MathUtilities.Clamp(dec_m, 0, 59);
        _dec_s = MathUtilities.Clamp(dec_s, 0.0, 59.999999);

        UpdateRADecimal();
        UpdateDECDecimal();
    }

    public EquatorialCoords()
    {
        RightAscension = 0;
        Declination = 0;
    }

    private void UpdateRAHoursMinutesSeconds()
    {
        _ra_h = (int)Math.Truncate(_ra * AstroUtilities.deg2hrs);
        _ra_m = (int)Math.Truncate((_ra * AstroUtilities.deg2hrs - _ra_h) * 60);
        _ra_s = (((_ra * AstroUtilities.deg2hrs - _ra_h) * 60) - _ra_m) * 60;
    }

    private void UpdateRADecimal()
    {
        if((_ra_h == 24) && (_ra_m > 0 || _ra_s > 0.0))
        {
            _ra_m = 0;
            _ra_s = 0.0;
        }

        _ra = _ra_h * AstroUtilities.hrs2deg + _ra_m / 4.0 + _ra_s / 240.0;
    }

    private void UpdateDECDegreesMinutesSeconds()
    {
        double abs_dec = Math.Abs(_dec);
        _dec_sign = (int)Math.Sign(_dec);
        _dec_d = (int)Math.Truncate(abs_dec);
        _dec_m = (int)Math.Truncate((abs_dec - _dec_d) * 60);
        _dec_s = (((abs_dec - _dec_d) * 60) - _dec_m) * 60;
    }

    private void UpdateDECDecimal()
    {
        if((_dec_d == 90 || _dec_d == -90) && (_dec_m > 0 || _dec_s > 0.0))
        {
            _dec_m = 0;
            _dec_s = 0.0;
        }

        _dec = _dec_d + _dec_m / 60.0 + _dec_s / 3600.0 * _dec_sign;
    }

    public string ToStringHoursMinutesSeconds(bool includeUnits = false)
    {
        if(includeUnits)
            return string.Format("{0:00}h {1:00}m {2:00.0000}s", _ra_h, _ra_m, _ra_s);
        else
            return string.Format("{0:00} {1:00} {2:00.0000}", _ra_h, _ra_m, _ra_s);
    }

    public string ToStringDegreesMinutesSeconds(bool includeUnits = false)
    {
        if(includeUnits)
            return string.Format("{0}{1:00}° {2:00}' {3:00.0000}\"", _dec_sign > 0 ? '+' : '–', _dec_d, _dec_m, _dec_s);
        else
            return string.Format("{0}{1:00} {2:00} {3:00.0000}", _dec_sign > 0 ? '+' : '–', _dec_d, _dec_m, _dec_s);
    }

    public static EquatorialCoords ToCoords(string coordString)
    {
        double ra = 0.0, dec = 0.0;
        double ra_h = 0.0, ra_m = 0.0, ra_s = 0.0;
        double dec_d = 0.0, dec_m = 0.0, dec_s = 0.0;

        MatchCollection mc;

        string pattern1 = "([0-9]+) +([0-9]+) +([-0-9]+(.[0-9]+)?),*[ +|\t*]([+|-]?[0-9]+) +([0-9]+) +([-0-9]+(.[0-9]+)?)";
        mc = Regex.Matches(coordString, pattern1);

        if(mc.Count > 0)
        {
            Match m = mc[0];

            ra_h = double.Parse(m.Groups[1].Value);
            ra_m = double.Parse(m.Groups[2].Value);
            ra_s = double.Parse(m.Groups[3].Value);

            ra = ra_h * AstroUtilities.hrs2deg + ra_m / 4.0 + ra_s / 240.0;

            dec_d = double.Parse(m.Groups[5].Value.Replace('–', '-'));
            dec_m = double.Parse(m.Groups[6].Value);
            dec_s = double.Parse(m.Groups[7].Value);

            dec = dec_d + (dec_m / 60.0 + dec_s / 3600.0) * Math.Sign(dec_d);

            //Engine.DebugLog("EquatorialCoords.ToCoords(): pattern 1 result = {0}h {1}m {2}s, {3}° {4}' {5}\"", ra_h, ra_m, ra_s, dec_d, dec_m, dec_s);
        }
        else
        {
            string pattern2 = "(([+|-|–]?[0-9]+(.[0-9]+)*)*h)? *(([0-9]+)*m)? *(([0-9]+(.[0-9]+)*)*s)? *, *(([+|-|–]?[0-9]+(.[0-9]+)*)*°)? *(([0-9]+)*['|′])? *(([0-9]+(.[0-9]+)*)*[\"|″])?";
            mc = Regex.Matches(coordString, pattern2);

            if(mc.Count > 0)
            {
                Match m = mc[0];

                ra_h = double.Parse(m.Groups[2].Value.Replace('–', '-'));
                ra_m = double.Parse(m.Groups[5].Value);
                ra_s = double.Parse(m.Groups[7].Value);

                ra = ra_h * AstroUtilities.hrs2deg + ra_m / 4.0 + ra_s / 240.0;

                dec_d = double.Parse(m.Groups[10].Value.Replace('–', '-'));
                dec_m = double.Parse(m.Groups[13].Value);
                dec_s = double.Parse(m.Groups[15].Value);

                dec = dec_d + (dec_m / 60.0 + dec_s / 3600.0) * Math.Sign(dec_d);

                //Engine.DebugLog("EquatorialCoords.ToCoords(): pattern 2 result = {0}h {1}m {2}s, {3}° {4}' {5}\"", ra_h, ra_m, ra_s, dec_d, dec_m, dec_s);
            }
            else
            {
                Engine.DebugLog("EquatorialCoords.ToCoords(): the coordinate string \"{0}\" is not a valid equatorial coordinate designation!", coordString);
            }
        }

        ra = MathUtilities.NormalizeAngle(ra);
        dec = MathUtilities.ClampAngle(dec, -90f, 90f);

        //Engine.DebugLog("EquatorialCoords.ToCoords(): decimal conversion = ({0:0.0000}, {1:0.0000})", ra, dec);

        return new EquatorialCoords(ra, dec);
    }

    /// <summary>
    /// Converts equatorial coordinates into horizontal coordinates at the specified julian date and observer latitude/longitude.
    /// </summary>
    /// <param name="equatorialCoords">The equatorial coordinates to be converted to horizontal coordinates.</param>
    /// <param name="julianDate">The julian date (time of observation).</param>
    /// <param name="latitude">The latitude of the horizontal reference plane (place of observation).</param>
    /// <param name="longitude">The longitude of the horizontal reference plane (place of observation).</param>
    /// <param name="respectRefraction">Defines whether to correct altitude values for refraction (true) or not.</param>
    /// <returns>The horizontal coordinates at the specified julian date and observer latitude/longitude.</returns>
    public static HorizontalCoords ToHorizontalCoords(EquatorialCoords equatorialCoords, double julianDate, float latitude, 
        float longitude, bool respectRefraction = false)
    {
        double localSiderealTimeInHours = AstroUtilities.GetLocalSiderealTime(julianDate, longitude);
        double localHourAngleInHours = (localSiderealTimeInHours - equatorialCoords.RightAscension * AstroUtilities.deg2hrs) % 24f;
        double localHourAngleInDegrees = localHourAngleInHours * AstroUtilities.hrs2deg;

        double altitudeSin = MathUtilities.Dsin(equatorialCoords.Declination) *
            MathUtilities.Dsin(latitude) + MathUtilities.Dcos(equatorialCoords.Declination) *
            MathUtilities.Dcos(latitude) * MathUtilities.Dcos(localHourAngleInDegrees);
        double altitude = MathUtilities.Dasin(altitudeSin);

        if(respectRefraction)
        {
            double refractionalDifferenceInArcMinutes = 1.02 / Math.Tan(altitudeSin + (10.3 / altitudeSin + 5.11));
            altitude += refractionalDifferenceInArcMinutes / 60.0;
            altitudeSin = altitude * Mathf.Deg2Rad;
        }

        double tan_y = -MathUtilities.Dsin(localHourAngleInDegrees);
        double tan_x = MathUtilities.Dtan(equatorialCoords.Declination) * MathUtilities.Dcos(latitude) -
            MathUtilities.Dsin(latitude) * MathUtilities.Dcos(localHourAngleInDegrees);

        double azimuth = 0f;

        double azimuthRad = 0f;
        if(tan_x != 0f)
        {
            azimuthRad = Math.Atan(tan_y / tan_x);
            azimuth = azimuthRad * MathUtilities.rad2deg;
            if(tan_x < 0.0)
                azimuth += 180.0;
            else if(azimuth < 0.0)
                azimuth += 360.0;
        }
        else
        {
            azimuth = tan_y < 0.0 ? 0.0 : 180.0;
        }

        return new HorizontalCoords(altitude, azimuth);
    }

    public HorizontalCoords ToHorizontalCoords(double julianDate, float latitude, float longitude, bool respectRefraction = false)
    {
        return ToHorizontalCoords(this, julianDate, latitude, longitude);
    }
}

public class CoordinateSet
{
    private HorizontalCoords hc;
    private EquatorialCoords ec;

    public HorizontalCoords HorizontalCoordinates {
        get { return hc; }
    }

    public EquatorialCoords EquatorialCoordinates {
        get { return ec; }
    }

    public CoordinateSet(HorizontalCoords hc, double julianDate, float latitude, float longitude, bool respectRefraction = false)
    {
        this.hc = hc;
        this.ec = hc.ToEquatorialCoords(julianDate, latitude, longitude, respectRefraction);
    }

    public CoordinateSet(EquatorialCoords ec, double julianDate, float latitude, float longitude, bool respectRefraction = false)
    {
        this.ec = ec;
        this.hc = ec.ToHorizontalCoords(julianDate, latitude, longitude, respectRefraction);
    }
}
