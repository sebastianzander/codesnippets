﻿/*
 * Author: Sebastian Zander
 *
 * The engine class that allowed control over simulation time, 
 * time zone switching, breakpoint checking and invocation, 
 * simple message logging and on-screen message output.
 */

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Engine : MonoBehaviour
{
    public class Breakpoint
    {
        public const int BP_FLAG_NONE = 0x0000;
        public const int BP_FLAG_EQUINOX = 0x0001;
        public const int BP_FLAG_SOLSTICE = 0x0002;
        public const int BP_FLAG_CUNJUNCTION = 0x0004;
        public const int BP_FLAG_OPPOSITION = 0x0008;
        public const int BP_FLAG_SWITCH_TO_SUMMER_TIME = 0x0010;
        public const int BP_FLAG_SWITCH_TO_WINTER_TIME = 0x0020;
        public const int BP_FLAG_ANNUAL = 0x1000;
        public const int BP_FLAG_ALL = BP_FLAG_EQUINOX | BP_FLAG_SOLSTICE | BP_FLAG_CUNJUNCTION | BP_FLAG_OPPOSITION;

        public double julianDate;
        public DateTime displayDate;
        public string displayName;
        public int flags;
        public bool triggered = false;
    }

    public class Message
    {
        public const float DEFAULT_MESSAGE_DISPLAY_DURATION = 8f;
        public const float MAX_MESSAGE_DISPLAY_DURATION = 30f;
        public const float DEFAULT_FADE_OUT_TIME = 2f;

        public int id;
        public GameObject messageObj;
        public float createdAt;
        public float removeAt;
        public bool isFading = false;
    }

    /* ATTRIBUTES */

    public static readonly int[] DAYS_IN_MONTH = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    public const int BITMASK_2 = 4 - 1;
    public const int BITMASK_3 = 8 - 1;
    public const int BITMASK_4 = 16 - 1;
    public const int BITMASK_5 = 32 - 1;
    public const int BITMASK_6 = 64 - 1;
    public const int BITMASK_7 = 128 - 1;
    public const int BITMASK_8 = 256 - 1;
    public const int BITMASK_9 = 512 - 1;

    public const float DAYS_PER_MONTH = 30.4368541f;
    public const float DAYS_PER_YEAR = 365.24225f;
    public const float JULIAN_DAYS_PER_YEAR = 365.25f;
    public const float SECONDS_PER_MINUTE = 60f;
    public const float SECONDS_PER_HOUR = SECONDS_PER_MINUTE * 60f;
    public const float SECONDS_PER_DAY = SECONDS_PER_HOUR * 24f;
    public const float SECONDS_PER_WEEK = SECONDS_PER_DAY * 7f;
    public const float SECONDS_PER_MONTH = SECONDS_PER_DAY * DAYS_PER_MONTH;
    public const float SECONDS_PER_YEAR = SECONDS_PER_DAY * DAYS_PER_YEAR;
    public const float MILLISECONDS_PER_DAY = SECONDS_PER_DAY * 1000f;
    //public static DateTime DATE_AT_J2000 = DateTime.ParseExact("2000/01/01 11:58:55.816", "yyyy/MM/dd HH:mm:ss.fff", null, System.Globalization.DateTimeStyles.AdjustToUniversal);
    public static DateTime DATE_AT_J2000 = DateTime.ParseExact("2000/01/01 12:00:00", "yyyy/MM/dd HH:mm:ss", null, System.Globalization.DateTimeStyles.AdjustToUniversal);
    public const double JULIAN_DATE_AT_J2000 = 2451545.0; // represents 2000-01-01 11:58:55.816 UTC or 2000-01-01 12:00:00 TT

    public const int MIN_TIME_SCALE_POWER_DEBUG = -3;
    public const int MIN_TIME_SCALE_POWER = 0;
    public const int MAX_TIME_SCALE_POWER = 32;
    public const float LAG_CORRECTION_MAX_DELTA_TIME = 0.05f;

    private static int timeScalePowerTarget = 8;
    private static float timeScaleTarget = Mathf.Pow(2, timeScalePowerTarget);

    private static float fadeInSecondsSimulation = 0.5f * SECONDS_PER_DAY;

    private static bool simulationPaused = false;
    private static bool simulationReverse = false;
    private static int timeScalePower = 0;
    private static float timeScale = 1f;
    private static double elapsedSeconds = 0.0;
    private static double elapsedSecondsInSimulation = 0.0;
    private static double elapsedSecondsInSimulationOffset = 0.0;
    private static DateTime dateInSimulation;
    private static DateTime dateAtProgramStart;
    private static double julianDateInSimulation;
    private static double julianDateAtProgramStart;
    private static double millisecondsSinceJ2000 = 0.0;
    private static double centuriesSinceJ2000 = 0.0;

    private static List<Breakpoint> breakpoints = new List<Breakpoint>();
    private static int enabledBreakpointFlags = 0;
    private static int[] breakpointYearRange = new int[2];
    private static Breakpoint summerTimeSwitchCheckpoint = new Breakpoint();
    private static Breakpoint winterTimeSwitchCheckpoint = new Breakpoint();

    private static Text timeDateLabel;
    private static Text timeScaleLabel;

    private static GameObject messagePanel;
    private static GameObject messagePrefab;
    private static List<Message> messages = new List<Message>();
    private static int messageCounter = 0;

    /* PROPERTIES */

    public static bool SimulationPaused { get; set; }
    public static bool SimulationReverse { get; set; }

    public static float TimeScale
    {
        get
        {
            return simulationPaused ? 0f : (simulationReverse ? -timeScale : timeScale);
        }
    }

    public static float TimeScaleUnpaused
    {
        get
        {
            return timeScale;
        }
    }

    public static double ElapsedSeconds
    {
        get
        {
            return elapsedSeconds;
        }
    }

    public static double ElapsedSecondsInSimulation
    {
        get
        {
            return elapsedSecondsInSimulation;
        }
    }

    public static DateTime DateInSimulation
    {
        get
        {
            return dateInSimulation;
        }
    }

    public static DateTime DateInEditor
    {
        get
        {
            return DateTime.ParseExact("2018/06/21 12:07:00", "yyyy/MM/dd HH:mm:ss", null, System.Globalization.DateTimeStyles.AdjustToUniversal);
        }
    }

    public static double JulianDateAtProgramStart
    {
        get
        {
            return julianDateAtProgramStart;
        }
    }

    public static double JulianDateInSimulation
    {
        get
        {
            if(julianDateInSimulation == 0)
                InitializeTime();
            return julianDateInSimulation;
        }
    }

    public static double CenturiesSinceJ2000
    {
        get
        {
            if(centuriesSinceJ2000 == 0.0)
            {
                TimeSpan timeSinceJ2000 = DateTime.UtcNow.Subtract(DATE_AT_J2000);
                millisecondsSinceJ2000 = timeSinceJ2000.TotalMilliseconds;
                centuriesSinceJ2000 = millisecondsSinceJ2000 / (SECONDS_PER_DAY * JULIAN_DAYS_PER_YEAR * 100);
                julianDateAtProgramStart = JULIAN_DATE_AT_J2000 + (millisecondsSinceJ2000 / MILLISECONDS_PER_DAY);
            }

            return centuriesSinceJ2000;
        }
    }

    public static double CenturiesSinceJ2000InEditor
    {
        get
        {
            TimeSpan timeSinceJ2000 = DateInEditor.Subtract(DATE_AT_J2000);
            millisecondsSinceJ2000 = timeSinceJ2000.TotalMilliseconds;
            return millisecondsSinceJ2000 / (SECONDS_PER_DAY * JULIAN_DAYS_PER_YEAR * 100);
        }
    }

    /* METHODS */

    // Use this for initialization
    void Start()
    {
        if(messagePrefab == null)
        {
            messagePrefab = Resources.Load("Prefabs/Message") as GameObject;
        }

        messagePanel = GameObject.Find("Message Panel");

        timeDateLabel = GameObject.Find("Time Date Label").GetComponent<Text>();
        if(timeDateLabel != null)
            timeDateLabel.text = "";

        timeScaleLabel = GameObject.Find("Time Scale Label").GetComponent<Text>();
        if(timeScaleLabel != null)
            timeScaleLabel.text = "";

        InitializeTime();

        timeScalePower = timeScalePowerTarget;
        timeScale = timeScaleTarget;

        enabledBreakpointFlags = Breakpoint.BP_FLAG_NONE;
        breakpointYearRange[0] = 2018;
        breakpointYearRange[1] = 2018;

        AddBreakpoint(DateTime.UtcNow, "Real now", -1);
        //AddBreakpoint("2018/06/15 12:00", "Debug breakpoint 1", -1);
        //AddBreakpoint("2018/09/12 00:00", "Easteregg breakpoint 1", -1);

        // add equinoxes and solstices as breakpoints
        AddBreakpoint("2018/03/20 16:15", "Vernal Equinox 2018", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2018/06/21 10:07", "Summer Solstice 2018", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2018/09/23 01:54", "Autumnal Equinox 2018", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2018/12/21 22:22", "Winter Solstice 2018", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2019/03/20 21:58", "Vernal Equinox 2019", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2019/06/21 15:54", "Summer Solstice 2019", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2019/09/23 07:50", "Autumnal Equinox 2019", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2019/12/22 04:19", "Winter Solstice 2019", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2020/03/20 03:49", "Vernal Equinox 2020", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2020/06/20 21:43", "Summer Solstice 2020", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2020/09/22 13:30", "Autumnal Equinox 2020", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2020/12/21 10:02", "Winter Solstice 2020", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2021/03/20 09:37", "Vernal Equinox 2021", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2021/06/21 03:32", "Summer Solstice 2021", Breakpoint.BP_FLAG_SOLSTICE);
        AddBreakpoint("2021/09/22 19:21", "Autumnal Equinox 2021", Breakpoint.BP_FLAG_EQUINOX);
        AddBreakpoint("2021/12/21 15:59", "Winter Solstice 2021", Breakpoint.BP_FLAG_SOLSTICE);

        // add forthcoming conjunctions as breakpoints
        AddBreakpoint("2018/10/14 15:06", "Venus/Mercury Conjunction", Breakpoint.BP_FLAG_CUNJUNCTION);
        AddBreakpoint("2018/10/30 03:45", "Jupiter/Mercury Conjunction", Breakpoint.BP_FLAG_CUNJUNCTION);
        AddBreakpoint("2019/01/22 05:43", "Jupiter/Venus Conjunction", Breakpoint.BP_FLAG_CUNJUNCTION);

        summerTimeSwitchCheckpoint.displayName = "Switch to summer time";
        summerTimeSwitchCheckpoint.flags = Breakpoint.BP_FLAG_SWITCH_TO_SUMMER_TIME;
        summerTimeSwitchCheckpoint.triggered = false;

        winterTimeSwitchCheckpoint.displayName = "Switch to winter time";
        winterTimeSwitchCheckpoint.flags = Breakpoint.BP_FLAG_SWITCH_TO_WINTER_TIME;
        winterTimeSwitchCheckpoint.triggered = false;

        UpdateTimeSwitchCheckpoints();
    }

    private void OnDestroy()
    {
        Location.Uninitialize();
    }

    public static void InitializeTime()
    {
        if(julianDateAtProgramStart == 0.0)
        {
            TimeSpan fadeInTimeSimulation = TimeSpan.FromSeconds(fadeInSecondsSimulation - Time.fixedDeltaTime * timeScaleTarget);
            elapsedSecondsInSimulationOffset = fadeInSecondsSimulation;

            dateAtProgramStart = DateTime.UtcNow.Subtract(fadeInTimeSimulation);
            dateInSimulation = dateAtProgramStart;
            TimeSpan timeSinceJ2000 = dateInSimulation.Subtract(DATE_AT_J2000);

            //Engine.DebugLog("Engine.InitializeTime(): simulation date at start: {0} UTC", dateInSimulation.ToString("yyyy/MM/dd HH:mm"));

            millisecondsSinceJ2000 = timeSinceJ2000.TotalMilliseconds;
            centuriesSinceJ2000 = millisecondsSinceJ2000 / (SECONDS_PER_DAY * JULIAN_DAYS_PER_YEAR * 100);
            julianDateAtProgramStart = JULIAN_DATE_AT_J2000 + (millisecondsSinceJ2000 / MILLISECONDS_PER_DAY);
            julianDateInSimulation = julianDateAtProgramStart;
        }
    }

    public static void ProgressTime(double secondsInSimulation, bool clearTrails)
    {
        elapsedSecondsInSimulation += secondsInSimulation;
        TimeSpan jumpTime = TimeSpan.FromSeconds(secondsInSimulation);
        dateInSimulation = dateInSimulation.Add(jumpTime);
        julianDateInSimulation = Engine.DateTimeToJulianDate(dateInSimulation);
        //UpdateTimeZoneOffset();
        Location.RefreshTimeZoneParameters();
        UpdateTimeSwitchCheckpoints();
        UpdateDateInSimulationLabel(true);

        //Engine.DebugLog("Engine.ProgressTime(): simulation date after progress: {0} UTC", dateInSimulation.ToString("yyyy/MM/dd HH:mm"));

        // clear all trails
        if(clearTrails)
        {
            Transform universe = GameObject.Find("Universe").transform;
            Body[] bodies = universe.GetComponentsInChildren<Body>(true);
            foreach(Body body in bodies)
                body.ClearTrail();
        }

        EventManager.InvokeEvent("ProgressTime");

        if(secondsInSimulation >= SECONDS_PER_HOUR * 2f)
            EventManager.InvokeEvent("UpdateAnalemma");
        else if(secondsInSimulation == SECONDS_PER_HOUR)
            EventManager.InvokeEvent("ClearAnalemma");
    }

    public static void ProgressTime(TimeSpan jumpTime, bool clearTrails)
    {
        elapsedSecondsInSimulation += jumpTime.TotalSeconds;
        dateInSimulation = dateInSimulation.Add(jumpTime);
        julianDateInSimulation = Engine.DateTimeToJulianDate(dateInSimulation);
        //UpdateTimeZoneOffset();
        Location.RefreshTimeZoneParameters();
        UpdateTimeSwitchCheckpoints();
        UpdateDateInSimulationLabel(true);

        //Engine.DebugLog("Engine.ProgressTime(): simulation date after progress: {0} UTC", dateInSimulation.ToString("yyyy/MM/dd HH:mm"));

        // clear all trails
        if(clearTrails)
        {
            Transform universe = GameObject.Find("Universe").transform;
            Body[] bodies = universe.GetComponentsInChildren<Body>(true);
            foreach(Body body in bodies)
                body.ClearTrail();
        }

        EventManager.InvokeEvent("ProgressTime");

        if(jumpTime.TotalSeconds >= SECONDS_PER_HOUR * 2f)
            EventManager.InvokeEvent("UpdateAnalemma");
        else if(jumpTime.TotalSeconds == SECONDS_PER_HOUR)
            EventManager.InvokeEvent("ClearAnalemma");
    }

    public static void SetDateInSimulation(DateTime targetDate)
    {
        TimeSpan diff = targetDate - dateInSimulation;
        ProgressTime(diff.TotalSeconds, diff.TotalSeconds > SECONDS_PER_DAY * 7f);
    }

    public static void SetDateInSimulationToNow(bool setRealtime = true, bool pauseSimulation = false)
    {
        SetDateInSimulation(DateTime.UtcNow);
        if(setRealtime)
        {
            timeScalePower = 0;
            timeScale = 1f;
        }
        simulationPaused = pauseSimulation;
    }

    public static void UpdateTimeZoneOffset()
    {
        // only switch clocks if this is supported for the current location
        if(Location.timeSwitchSupported)
        {
            int year = dateInSimulation.Year;

            DateTime lastOfMarch = DateTime.ParseExact(year + "/03/31", "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.AdjustToUniversal);
            DateTime lastOfOctober = DateTime.ParseExact(year + "/10/31", "yyyy/MM/dd", null, System.Globalization.DateTimeStyles.AdjustToUniversal);

            int dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
            int dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;

            DateTime lastSundayInMarch = new DateTime(year, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);
            DateTime lastSundayInOctober = new DateTime(year, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);

            bool logUpdates = false;

            // if current date in simulation is within summer time
            if(dateInSimulation.Ticks >= lastSundayInMarch.Ticks && dateInSimulation.Ticks < lastSundayInOctober.Ticks &&
                Location.timeZoneOffsetInSeconds != Location.timeZoneOffsetSummerInSeconds)
            {
                Location.timeZoneName = Location.timeZoneSummerName;
                Location.timeZoneShortName = Location.timeZoneSummerShortName;
                Location.timeZoneOffsetInSeconds = Location.timeZoneOffsetSummerInSeconds;

                if(logUpdates)
                    Engine.DebugLog("Engine.UpdateTimeZoneOffset(): updated time zone offset according to summer time");

                AddMessage("Switched to summer time");
            }

            // otherwise current date in simulation is within winter time
            else if((dateInSimulation.Ticks < lastSundayInMarch.Ticks || dateInSimulation.Ticks > lastSundayInOctober.Ticks) &&
                Location.timeZoneOffsetInSeconds != Location.timeZoneOffsetWinterInSeconds)
            {
                Location.timeZoneName = Location.timeZoneWinterName;
                Location.timeZoneShortName = Location.timeZoneWinterShortName;
                Location.timeZoneOffsetInSeconds = Location.timeZoneOffsetWinterInSeconds;

                if(logUpdates)
                    Engine.DebugLog("Engine.UpdateTimeZoneOffset(): updated time zone offset according to winter time");

                AddMessage("Switched to <color=cyan>winter</color> time");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        float lastTimeScale = TimeScale;
        bool lastSimulationPaused = simulationPaused;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        double progressSecondsInSimulation = 0.0;
        double progressTimeMultiplier = 1.0;
        float progressTimeSign = 1f;
        int progressTimeValue = 0;

        // progress hours
        progressTimeMultiplier = SECONDS_PER_HOUR;

        // progress days
        if((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
            progressTimeMultiplier = SECONDS_PER_DAY;

        // progress months
        if((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
            progressTimeMultiplier = SECONDS_PER_DAY * 30f; // assuming 30 full days for leaving the day time unchanged

        // progress years
        if((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && ((Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))))
            progressTimeMultiplier = SECONDS_PER_DAY * DAYS_PER_YEAR;

        // progress reversely
        if(Input.GetKey(KeyCode.LeftAlt))
            progressTimeSign = -1f;

        if(Input.GetKeyDown(KeyCode.Keypad1))
            progressTimeValue = 1;
        else if(Input.GetKeyDown(KeyCode.Keypad2))
            progressTimeValue = 2;
        else if(Input.GetKeyDown(KeyCode.Keypad3))
            progressTimeValue = 3;
        else if(Input.GetKeyDown(KeyCode.Keypad4))
            progressTimeValue = 4;
        else if(Input.GetKeyDown(KeyCode.Keypad5))
            progressTimeValue = 5;
        else if(Input.GetKeyDown(KeyCode.Keypad6))
            progressTimeValue = 6;
        else if(Input.GetKeyDown(KeyCode.Keypad7))
            progressTimeValue = 7;

        if((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKeyDown(KeyCode.End))
        {
            progressTimeMultiplier = SECONDS_PER_DAY * DAYS_PER_YEAR;
            progressTimeValue = 1;
        }
        else if(Input.GetKeyDown(KeyCode.End))
        {
            progressTimeMultiplier = SECONDS_PER_DAY * DAYS_PER_MONTH;
            progressTimeValue = 1;
        }

        progressSecondsInSimulation = progressTimeValue * progressTimeSign * progressTimeMultiplier;

        if(progressTimeValue > 0)
            Engine.ProgressTime(progressSecondsInSimulation, progressSecondsInSimulation >= SECONDS_PER_DAY * 14f);

        //if(Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.LeftControl) && fadeInSecondsRealRemaining <= 0f)
        //{
        //    simulationStep = true;
        //    Engine.DebugLog("Engine.Update(): simulation date at step: {0} UTC", dateInSimulation.ToString("yyyy/MM/dd HH:mm"));
        //}
        //else
        if(Input.GetKeyDown(KeyCode.Space) /*&& hitRealNow*/)
        {
            simulationPaused = !simulationPaused;
            //if(simulationPaused)
            //    Engine.DebugLog("Engine.Update(): simulation date at pause: {0} UTC", dateInSimulation.ToString("yyyy/MM/dd HH:mm"));
        }

        if(Input.GetKeyDown(KeyCode.KeypadPeriod) && Input.GetKey(KeyCode.LeftControl))
        {
            simulationReverse = !simulationReverse;
            UpdateTimeSwitchCheckpoints();
        }

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            timeScalePower = 0;
            timeScale = 1f;
        }

        if(Input.GetKeyDown(KeyCode.KeypadPlus) || Input.GetKeyDown(KeyCode.Plus))
        {
            timeScalePower++;
            if(timeScalePower > MAX_TIME_SCALE_POWER)
                timeScalePower = MAX_TIME_SCALE_POWER;
            timeScale = Mathf.Pow(2, timeScalePower);
        }

        if(Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Minus))
        {
            timeScalePower--;
            if(Input.GetKey(KeyCode.LeftControl) && timeScalePower < MIN_TIME_SCALE_POWER_DEBUG)
                timeScalePower = MIN_TIME_SCALE_POWER_DEBUG;
            else if(!Input.GetKey(KeyCode.LeftControl) && timeScalePower < MIN_TIME_SCALE_POWER)
                timeScalePower = MIN_TIME_SCALE_POWER;
            timeScale = Mathf.Pow(2, timeScalePower);
        }

        UpdateTimeScaleLabel(lastTimeScale, lastSimulationPaused);

        if(!simulationPaused)
        {
            double deltaElapsedSecondsInReal = Time.deltaTime;
            double deltaElapsedSecondsInSimulation = Time.deltaTime * TimeScale;
            double newJulianDateInSimulation = julianDateAtProgramStart + (elapsedSecondsInSimulation + deltaElapsedSecondsInSimulation) / SECONDS_PER_DAY;

            breakpoints.RemoveAll(item => item.triggered);

            bool enableSpecificDstHandling = false;

            // specific daylight saving time handling
            // only switch clocks if this is supported for the current location
            if(Location.timeSwitchSupported && enableSpecificDstHandling)
            {
                bool logUpdates = false;

                if((!simulationReverse && MathUtilities.IsBetween(summerTimeSwitchCheckpoint.julianDate, julianDateInSimulation, newJulianDateInSimulation)) ||
                    (simulationReverse && MathUtilities.IsBetween(winterTimeSwitchCheckpoint.julianDate, julianDateInSimulation, newJulianDateInSimulation)))
                {
                    Location.timeZoneName = Location.timeZoneSummerName;
                    Location.timeZoneShortName = Location.timeZoneSummerShortName;
                    Location.timeZoneOffsetInSeconds = Location.timeZoneOffsetSummerInSeconds;
                    UpdateTimeSwitchCheckpoints();

                    if(logUpdates)
                        Engine.DebugLog("Engine.FixedUpdate(): switched to summer time");

                    if(timeScalePower <= 18)
                        AddMessage("Switched to summer time");
                }

                if((!simulationReverse && MathUtilities.IsBetween(winterTimeSwitchCheckpoint.julianDate, julianDateInSimulation, newJulianDateInSimulation)) ||
                    (simulationReverse && MathUtilities.IsBetween(summerTimeSwitchCheckpoint.julianDate, julianDateInSimulation, newJulianDateInSimulation)))
                {
                    Location.timeZoneName = Location.timeZoneWinterName;
                    Location.timeZoneShortName = Location.timeZoneWinterShortName;
                    Location.timeZoneOffsetInSeconds = Location.timeZoneOffsetWinterInSeconds;
                    UpdateTimeSwitchCheckpoints();

                    if(logUpdates)
                        Engine.DebugLog("Engine.FixedUpdate(): switched to winter time");

                    if(timeScalePower <= 18)
                        AddMessage("Switched to winter time");
                }
            }

            DateTime newDateInSimulation = JulianDateToDateTime(newJulianDateInSimulation);
            Location.Update(dateInSimulation, newDateInSimulation);

            // generic daylight saving time handling
            //if(Location.currentDaylightSavingTimeInformation != null)
            //{
            //    DateTime newDateInSimulation = JulianDateToDateTime(newJulianDateInSimulation);
            //    DateTime nextDstStartDate = DateTime.MinValue;

            //    if(Location.currentDaylightSavingTimeInformation.dstStartDateMode == DaylightSavingTimeDateMode.DSTDM_LAST_SPECIFIC_WEEKDAY_OF_MONTH)
            //    {
            //        int dstStartDay     = 0;
            //        int dstStartWeekday = (Location.currentDaylightSavingTimeInformation.dstStartData      ) & 7;
            //        int dstStartMonth   = (Location.currentDaylightSavingTimeInformation.dstStartData >>  3) & 31;
            //        int dstStartHour    = (Location.currentDaylightSavingTimeInformation.dstStartData >>  8) & 31;
            //        int dstStartMin     = (Location.currentDaylightSavingTimeInformation.dstStartData >> 12) & 63;
            //        int dstStartYear    = dateInSimulation.Year;

            //        Debug.Assert(dstStartMonth >= 1 && dstStartMonth <= 12);

            //        int lastDayOfMonth = DAYS_IN_MONTH[dstStartMonth-1];
            //        DateTime lastOfMonth = new DateTime(dstStartYear, dstStartMonth, lastDayOfMonth, 0, 0, 0, DateTimeKind.Utc);
            //        dstStartDay = lastDayOfMonth - ((int)lastOfMonth.DayOfWeek - dstStartWeekday) % 7;
            //        nextDstStartDate = new DateTime(dstStartYear, dstStartMonth, dstStartDay, dstStartHour, dstStartMin, 0, DateTimeKind.Utc);

            //        if(nextDstStartDate.Ticks < dateInSimulation.Ticks)
            //        {
            //            dstStartYear++;
            //            lastOfMonth = new DateTime(dstStartYear, dstStartMonth, lastDayOfMonth, 0, 0, 0, DateTimeKind.Utc);
            //            dstStartDay = lastDayOfMonth - ((int)lastOfMonth.DayOfWeek - dstStartWeekday) % 7;
            //            nextDstStartDate = new DateTime(dstStartYear, dstStartMonth, dstStartDay, dstStartHour, dstStartMin, 0, DateTimeKind.Utc);
            //        }
            //    }

            //    if(nextDstStartDate != DateTime.MinValue && MathUtilities.IsBetween(nextDstStartDate.Ticks, dateInSimulation.Ticks, newDateInSimulation.Ticks))
            //    {
            //        float hours = Mathf.Abs(Location.currentDaylightSavingTimeInformation.dstTimeZoneOffset - Location.currentDaylightSavingTimeInformation.stdTimeZoneOffset) / 3600f;
            //        AddMessage(15f, "Turned local clock <color=white>{0} by {1} {2}</color>\n" +
            //            "<color=grey>Changed time zone to {3}</color>", 
            //            Location.currentDaylightSavingTimeInformation.dstTimeZoneOffset < Location.currentDaylightSavingTimeInformation.stdTimeZoneOffset ? "backward" : "forward",
            //            hours, hours == 1f ? "hour" : "hours", Location.currentDaylightSavingTimeInformation.dstTimeZoneName);

            //        Location.timeZoneName = Location.currentDaylightSavingTimeInformation.dstTimeZoneName;
            //        Location.timeZoneShortName = Location.currentDaylightSavingTimeInformation.dstTimeZoneShortName;
            //        Location.timeZoneOffsetInSeconds = Location.currentDaylightSavingTimeInformation.dstTimeZoneOffset;
            //    }

            //    DateTime nextDstEndDate = DateTime.MinValue;

            //    if(Location.currentDaylightSavingTimeInformation.dstEndDateMode == DaylightSavingTimeDateMode.DSTDM_LAST_SPECIFIC_WEEKDAY_OF_MONTH)
            //    {
            //        int dstEndDay     = 0;
            //        int dstEndWeekday = (Location.currentDaylightSavingTimeInformation.dstEndData      ) & 7;
            //        int dstEndMonth   = (Location.currentDaylightSavingTimeInformation.dstEndData >>  3) & 31;
            //        int dstEndHour    = (Location.currentDaylightSavingTimeInformation.dstEndData >>  8) & 31;
            //        int dstEndMin     = (Location.currentDaylightSavingTimeInformation.dstEndData >> 12) & 63;
            //        int dstEndYear    = dateInSimulation.Year;

            //        Debug.Assert(dstEndMonth >= 1 && dstEndMonth <= 12);

            //        int lastDayOfMonth = DAYS_IN_MONTH[dstEndMonth-1];
            //        DateTime lastOfMonth = new DateTime(dstEndYear, dstEndMonth, lastDayOfMonth, 0, 0, 0, DateTimeKind.Utc);
            //        dstEndDay = lastDayOfMonth - ((int)lastOfMonth.DayOfWeek - dstEndWeekday) % 7;
            //        nextDstEndDate = new DateTime(dstEndYear, dstEndMonth, dstEndDay, dstEndHour, dstEndMin, 0, DateTimeKind.Utc);

            //        if(nextDstEndDate.Ticks < dateInSimulation.Ticks)
            //        {
            //            dstEndYear++;
            //            lastOfMonth = new DateTime(dstEndYear, dstEndMonth, lastDayOfMonth, 0, 0, 0, DateTimeKind.Utc);
            //            dstEndDay = lastDayOfMonth - ((int)lastOfMonth.DayOfWeek - dstEndWeekday) % 7;
            //            nextDstEndDate = new DateTime(dstEndYear, dstEndMonth, dstEndDay, dstEndHour, dstEndMin, 0, DateTimeKind.Utc);
            //        }
            //    }

            //    if(nextDstEndDate != DateTime.MinValue && MathUtilities.IsBetween(nextDstEndDate.Ticks, dateInSimulation.Ticks, newDateInSimulation.Ticks))
            //    {
            //        float hours = Mathf.Abs(Location.currentDaylightSavingTimeInformation.dstTimeZoneOffset - Location.currentDaylightSavingTimeInformation.stdTimeZoneOffset) / 3600f;
            //        AddMessage(15f, "Turned local clock <color=white>{0} by {1} {2}</color>\n" +
            //            "<color=grey>Changed time zone to {3}</color>",
            //            Location.currentDaylightSavingTimeInformation.dstTimeZoneOffset > Location.currentDaylightSavingTimeInformation.stdTimeZoneOffset ? "backward" : "forward",
            //            hours, hours == 1f ? "hour" : "hours", Location.currentDaylightSavingTimeInformation.stdTimeZoneName);

            //        Location.timeZoneName = Location.currentDaylightSavingTimeInformation.stdTimeZoneName;
            //        Location.timeZoneShortName = Location.currentDaylightSavingTimeInformation.stdTimeZoneShortName;
            //        Location.timeZoneOffsetInSeconds = Location.currentDaylightSavingTimeInformation.stdTimeZoneOffset;
            //    }
            //}

            // check for timed breakpoints, possibly adjust the simulation dates and pause the simulation
            foreach(Breakpoint breakpoint in breakpoints)
            {
                if(breakpoint.displayName.Equals("Real now") && !breakpoint.triggered)
                {
                    // update the breakpoint while progressing towards real now
                    UpdateBreakpoint(breakpoint, DateTime.UtcNow);
                }

                if(!breakpoint.triggered && breakpoint.julianDate >= julianDateInSimulation && breakpoint.julianDate <= newJulianDateInSimulation &&
                    (breakpoint.flags == -1 || (enabledBreakpointFlags & breakpoint.flags) != 0 && breakpoint.displayDate.Year >= breakpointYearRange[0] && breakpoint.displayDate.Year <= breakpointYearRange[1]))
                {
                    // get the precise delta times so the simulation doesn't jump its regular, probably huge simulation delta time
                    double deltaJulianDateInSimulation = breakpoint.julianDate - julianDateInSimulation;
                    deltaElapsedSecondsInSimulation = deltaJulianDateInSimulation * SECONDS_PER_DAY;
                    deltaElapsedSecondsInReal = deltaElapsedSecondsInSimulation / timeScale;

                    //Engine.DebugLog("triggered breakpoint: {0}", breakpoint.displayName);
                    Engine.DebugLog("Engine.FixedUpdate(): simulation date at breakpoint: {0} UTC ({1})",
                        dateAtProgramStart.AddSeconds(elapsedSecondsInSimulation + deltaElapsedSecondsInSimulation).ToString("yyyy/MM/dd HH:mm"),
                        breakpoint.displayName);

                    if(breakpoint.displayName.Equals("Real now"))
                    {
                        elapsedSecondsInSimulationOffset = elapsedSecondsInSimulation + deltaElapsedSecondsInSimulation;
                        timeScalePower = 0;
                        timeScale = 1f;
                        //simulationPaused = true;

                        UpdateTimeScaleLabel(0f, false);
                    }
                    else
                    {
                        simulationPaused = true;
                    }

                    breakpoint.triggered = true;
                    break;
                }
            }

            elapsedSeconds += deltaElapsedSecondsInReal;
            elapsedSecondsInSimulation += deltaElapsedSecondsInSimulation;

            // update date/time and julian date in simulation
            dateInSimulation = dateAtProgramStart.AddSeconds(elapsedSecondsInSimulation);
            julianDateInSimulation = julianDateAtProgramStart + (elapsedSecondsInSimulation) / SECONDS_PER_DAY;
        }
        else
        {
            Location.Update(dateInSimulation, DateTime.MinValue);
        }

        UpdateDateInSimulationLabel(true);
        UpdateMessages();
    }

    private static void UpdateMessages()
    {
        foreach(Message message in messages)
        {
            if(message.messageObj != null)
            {
                if(Time.realtimeSinceStartup > message.removeAt - Message.DEFAULT_FADE_OUT_TIME && !message.isFading)
                {
                    Text messageText = message.messageObj.GetComponent<Text>();
                    if(messageText != null)
                        messageText.CrossFadeAlpha(0f, Message.DEFAULT_FADE_OUT_TIME, true);
                    message.isFading = true;
                    GameObject.Destroy(message.messageObj, Message.DEFAULT_FADE_OUT_TIME + 0.2f);
                }
            }
        }

        messages.RemoveAll(item => item.removeAt < Time.realtimeSinceStartup + 1f);
    }

    private static void UpdateDateInSimulationLabel(bool includeAge = true)
    {
        string dateFormatString = "yyyy-MM-dd  HH:mm:ss  UTC";
        if(timeScale > 86400f)
            dateFormatString = "yyyy-MM-dd  UTC";
        else if(timeScale > 60f)
            dateFormatString = "yyyy-MM-dd  HH:mm  UTC";

        if(timeDateLabel != null)
        {
            timeDateLabel.text = "TIME:  " + dateInSimulation.ToString(dateFormatString);

            double elapsedSimulationSecondsAdjusted = elapsedSecondsInSimulation - elapsedSecondsInSimulationOffset;
            if(includeAge && elapsedSecondsInSimulationOffset > 0.0)
            {
                if(elapsedSimulationSecondsAdjusted > SECONDS_PER_YEAR * 9.95 || elapsedSimulationSecondsAdjusted < -SECONDS_PER_YEAR * 9.95)
                    timeDateLabel.text += string.Format("    <color=#739BB2>{0:0} YEARS</color>", elapsedSimulationSecondsAdjusted / SECONDS_PER_YEAR);
                else if(elapsedSimulationSecondsAdjusted >= SECONDS_PER_YEAR || elapsedSimulationSecondsAdjusted <= -SECONDS_PER_YEAR)
                    timeDateLabel.text += string.Format("    <color=#739BB2>{0:0.0} YEARS</color>", elapsedSimulationSecondsAdjusted / SECONDS_PER_YEAR);
                else
                    timeDateLabel.text += string.Format("    <color=#739BB2>{0:0} DAYS</color>", Math.Floor(elapsedSimulationSecondsAdjusted / SECONDS_PER_DAY));
            }
        }
    }

    private void UpdateTimeScaleLabel(float lastTimeScale, bool lastSimulationPaused)
    {
        if(timeScaleLabel == null)
            return;

        //if(lastTimeScale != TimeScale || lastSimulationPaused != simulationPaused || timeScaleLabel.text == "")
        {
            if(simulationReverse)
            {
                if(timeScale == 1f)
                    timeScaleLabel.text = "TIME SCALE:  REVERSE REALTIME";
                else if(timeScale <= 512)
                    timeScaleLabel.text = string.Format("TIME SCALE:  −{0}X", timeScale);
                else
                    timeScaleLabel.text = string.Format("TIME SCALE:  -(2^{0}X)", timeScalePower);
            }
            else
            {
                if(timeScale == 1f)
                    timeScaleLabel.text = "TIME SCALE:  REALTIME";
                else if(timeScale <= 512)
                    timeScaleLabel.text = string.Format("TIME SCALE:  {0}X", timeScale);
                else
                    timeScaleLabel.text = string.Format("TIME SCALE:  2^{0}X", timeScalePower);
            }

            if(simulationPaused)
                timeScaleLabel.text += "  (PAUSED)";
        }
    }

    public static void DebugLog(string format, params System.Object[] args)
    {
        Debug.Log(string.Format("<color=white>Debug:</color> " + format, args));
    }

    public static void AddMessage(string format, params System.Object[] args)
    {
        AddMessage(Message.DEFAULT_MESSAGE_DISPLAY_DURATION, format, args);
    }

    public static void AddMessage(float displayDuration, string format, params System.Object[] args)
    {
        if(messagePrefab == null || messagePanel == null)
            return;

        displayDuration = Mathf.Clamp(displayDuration, 0f, Message.MAX_MESSAGE_DISPLAY_DURATION);

        Message message = new Message();
        message.id = messageCounter++;
        message.createdAt = Time.realtimeSinceStartup;
        message.removeAt = message.createdAt + displayDuration;
        message.messageObj = Instantiate(messagePrefab, messagePanel.transform);

        if(message.messageObj != null)
        {
            //Engine.DebugLog("Engine.AddMessage(): added message id {0} (created at: {1}; remove at: {2})",
            //    message.id, message.createdAt, message.removeAt);

            message.messageObj.name = string.Format("Message{0:0000}", message.id);

            Text messageText = message.messageObj.GetComponent<Text>();
            if(messageText != null)
            {
                if(format.StartsWith("{log}", StringComparison.CurrentCultureIgnoreCase))
                {
                    format = format.Substring(5);
                    string formatted = string.Format(format, args);
                    messageText.text = formatted.ToUpper();

                    DebugLog("Engine.AddMessage(relay): " + formatted);
                }
                else
                {
                    messageText.text = string.Format(format, args).ToUpper();
                }

                //TextGenerator textGenerator = new TextGenerator();
                //TextGenerationSettings settings = messageText.GetGenerationSettings(messageText.rectTransform.rect.size);
                //float width = textGenerator.GetPreferredWidth(messageText.text, settings);
                //float height = textGenerator.GetPreferredHeight(messageText.text, settings);

                float height = LayoutUtility.GetPreferredHeight(messageText.GetComponent<RectTransform>());

                // go through all currently displaying messages, shift them towards the bottom and shrink them
                // to make place for the new message
                for(int i = messages.Count - 1; i >= 0; i--)
                {
                    Message otherMessage = messages[i];

                    if(otherMessage.messageObj != null)
                    {
                        RectTransform rect = otherMessage.messageObj.GetComponent<RectTransform>();
                        if(rect != null)
                        {
                            rect.anchoredPosition = new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y - height);
                            height = LayoutUtility.GetPreferredHeight(rect);
                        }

                        Text otherMessageText = otherMessage.messageObj.GetComponent<Text>();
                        if(otherMessageText != null && otherMessageText.fontSize == messageText.fontSize)
                        {
                            otherMessageText.fontSize = (int)(otherMessageText.fontSize * 0.7f);
                        }
                    }
                }

                messages.Add(message);
            }
        }
    }

    public static string TimeToString(double time, double transition = 1.1f, string numericFormat = "0.00")
    {
        string timeString;

        if(time >= SECONDS_PER_YEAR * transition)
        {
            timeString = string.Format("{0:" + numericFormat + "} years", time / SECONDS_PER_YEAR);
        }
        else if(time >= SECONDS_PER_DAY * transition)
        {
            timeString = string.Format("{0:" + numericFormat + "} days", time / SECONDS_PER_DAY);
        }
        else if(time >= SECONDS_PER_HOUR * transition)
        {
            timeString = string.Format("{0:" + numericFormat + "} hours", time / SECONDS_PER_HOUR);
        }
        else if(time >= SECONDS_PER_MINUTE * transition)
        {
            timeString = string.Format("{0:" + numericFormat + "} minutes", time / SECONDS_PER_MINUTE);
        }
        else
        {
            timeString = string.Format("{0:" + numericFormat + "} seconds", time);
        }

        return timeString;
    }

    public static double JulianDateToCenturiesSinceJ2000(double julianDate)
    {
        return (julianDate - JULIAN_DATE_AT_J2000) / JULIAN_DAYS_PER_YEAR / 100.0;
    }

    public static double DateTimeToJulianDate(DateTime date)
    {
        TimeSpan timeSinceJ2000 = date.Subtract(DATE_AT_J2000);
        return timeSinceJ2000.TotalDays + JULIAN_DATE_AT_J2000;
    }

    public static DateTime JulianDateToDateTime(double julianDate)
    {
        // calculate julian days since J2000
        double days = julianDate - JULIAN_DATE_AT_J2000;
        return DATE_AT_J2000.AddDays(days);
    }

    public static double CenturiesSinceJ2000ToJulianDate(double centuriesSinceJ2000)
    {
        double daysSinceJ2000 = centuriesSinceJ2000 * 36525;
        return JULIAN_DATE_AT_J2000 + daysSinceJ2000;
    }

    public static double GetSecondsSinceJ2000(DateTime date)
    {
        TimeSpan time = date.Subtract(DATE_AT_J2000);
        return time.TotalSeconds;
    }

    public static Breakpoint AddBreakpoint(Breakpoint breakpoint)
    {
        breakpoints.Add(breakpoint);
        return breakpoint;
    }

    public static Breakpoint AddBreakpoint(DateTime displayDate, string displayName = "", int flags = Breakpoint.BP_FLAG_ALL)
    {
        Breakpoint breakpoint = new Breakpoint();
        breakpoint.displayDate = displayDate;
        breakpoint.displayName = displayName;
        breakpoint.flags = flags;
        breakpoint.julianDate = DateTimeToJulianDate(displayDate);

        return AddBreakpoint(breakpoint);
    }

    public static Breakpoint AddBreakpoint(string displayDateString, string displayName = "", int flags = Breakpoint.BP_FLAG_ALL)
    {
        DateTime displayDate = DateTime.ParseExact(displayDateString, "yyyy/MM/dd HH:mm", null, System.Globalization.DateTimeStyles.AdjustToUniversal);
        return AddBreakpoint(displayDate, displayName, flags);
    }

    public static Breakpoint AddBreakpoint(double julianDate, string displayName = "", int flags = Breakpoint.BP_FLAG_ALL)
    {
        Breakpoint breakpoint = new Breakpoint();
        breakpoint.displayName = displayName;
        breakpoint.julianDate = julianDate;
        breakpoint.flags = flags;
        breakpoint.displayDate = JulianDateToDateTime(julianDate);

        return AddBreakpoint(breakpoint);
    }

    public static Breakpoint AddBreakpoint(double julianDate, DateTime displayDate, string displayName = "", int flags = Breakpoint.BP_FLAG_ALL)
    {
        Breakpoint breakpoint = new Breakpoint();
        breakpoint.displayName = displayName;
        breakpoint.julianDate = julianDate;
        breakpoint.displayDate = displayDate;
        breakpoint.flags = flags;

        return AddBreakpoint(breakpoint);
    }

    public static void UpdateBreakpoint(Breakpoint breakpoint, DateTime displayDate)
    {
        if(breakpoint == null)
            return;

        breakpoint.displayDate = displayDate;
        breakpoint.julianDate = DateTimeToJulianDate(displayDate);
    }

    public static bool IsBreakpointSet(DateTime displayDate, int flags)
    {
        bool set = false;
        foreach(Breakpoint breakpoint in breakpoints)
        {
            if(breakpoint.displayDate.Equals(displayDate) && breakpoint.flags == flags)
            {
                set = true;
                break;
            }
        }
        return set;
    }

    public static void UpdateTimeSwitchCheckpoints()
    {
        int year = dateInSimulation.Year;

        DateTime lastOfMarch = new DateTime(year, 3, 31, 0, 0, 0, DateTimeKind.Utc);
        int dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
        DateTime lastSundayInMarch = new DateTime(year, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);

        DateTime lastOfOctober = new DateTime(year, 10, 31, 0, 0, 0, DateTimeKind.Utc);
        int dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;
        DateTime lastSundayInOctober = new DateTime(year, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);

        // if within last years/this years winter days
        if(dateInSimulation.Ticks < lastSundayInMarch.Ticks)
        {
            if(simulationReverse)
            {
                // update last sunday in March to last year
                lastOfMarch = new DateTime(year - 1, 3, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
                lastSundayInMarch = new DateTime(year - 1, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);

                // update last sunday in October to last year
                lastOfOctober = new DateTime(year - 1, 10, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;
                lastSundayInOctober = new DateTime(year - 1, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);
            }
        }

        // if within this years summer days
        else if(dateInSimulation.Ticks >= lastSundayInMarch.Ticks && dateInSimulation.Ticks < lastSundayInOctober.Ticks)
        {
            if(!simulationReverse)
            {
                // update last sunday in March to next year
                lastOfMarch = new DateTime(year + 1, 3, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
                lastSundayInMarch = new DateTime(year + 1, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);
            }
            else
            {
                // update last sunday in October to last year
                lastOfOctober = new DateTime(year - 1, 10, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;
                lastSundayInOctober = new DateTime(year - 1, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);
            }
        }

        // if within this years/next years winter days
        else if(dateInSimulation.Ticks >= lastSundayInOctober.Ticks)
        {
            if(!simulationReverse)
            {
                // update last sunday in March to next year
                lastOfMarch = new DateTime(year + 1, 3, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
                lastSundayInMarch = new DateTime(year + 1, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);

                // update last sunday in October to next year
                lastOfOctober = new DateTime(year + 1, 10, 31, 0, 0, 0, DateTimeKind.Utc);
                dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;
                lastSundayInOctober = new DateTime(year + 1, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);
            }
        }

        bool logUpdates = false;

        // only log updates if values actually changed
        if(!summerTimeSwitchCheckpoint.displayDate.Equals(lastSundayInMarch) &&
            !winterTimeSwitchCheckpoint.displayDate.Equals(lastSundayInOctober) && logUpdates)
        {
            Engine.DebugLog("Engine.UpdateTimeSwitchCheckpoints(): updated summer time date to {0} and winter time date to {1}",
                lastSundayInMarch.ToString("yyyy/MM/dd"), lastSundayInOctober.ToString("yyyy/MM/dd"));
        }
        else if(!summerTimeSwitchCheckpoint.displayDate.Equals(lastSundayInMarch) && logUpdates)
        {
            Engine.DebugLog("Engine.UpdateTimeSwitchCheckpoints(): updated summer time date to {0}",
                lastSundayInMarch.ToString("yyyy/MM/dd"));
        }
        else if(!winterTimeSwitchCheckpoint.displayDate.Equals(lastSundayInOctober) && logUpdates)
        {
            Engine.DebugLog("Engine.UpdateTimeSwitchCheckpoints(): updated winter time date to {0}",
                lastSundayInOctober.ToString("yyyy/MM/dd"));
        }

        // update actual time switch checkpoints
        UpdateBreakpoint(summerTimeSwitchCheckpoint, lastSundayInMarch);
        UpdateBreakpoint(winterTimeSwitchCheckpoint, lastSundayInOctober);
    }

    public static void UpdateTimeSwitchSummerCheckpoint(int year)
    {
        DateTime lastOfMarch = new DateTime(year, 3, 31, 0, 0, 0, DateTimeKind.Utc);
        int dayLastSundayInMarch = 31 - (int)lastOfMarch.DayOfWeek;
        DateTime lastSundayInMarch = new DateTime(year, 3, dayLastSundayInMarch, 0, 0, 0, DateTimeKind.Utc);

        UpdateBreakpoint(summerTimeSwitchCheckpoint, lastSundayInMarch);
    }

    public static void UpdateTimeSwitchWinterCheckpoint(int year)
    {
        DateTime lastOfOctober = new DateTime(year, 10, 31, 0, 0, 0, DateTimeKind.Utc);
        int dayLastSundayInOctober = 31 - (int)lastOfOctober.DayOfWeek;
        DateTime lastSundayInOctober = new DateTime(year, 10, dayLastSundayInOctober, 0, 0, 0, DateTimeKind.Utc);

        UpdateBreakpoint(winterTimeSwitchCheckpoint, lastSundayInOctober);
    }
}
