using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEngine;

public enum AstronomicalEpoch
{
    AE_J2000
}

public enum OrbitalElementUnit
{
    EU_NONE, 
    EU_RAD,
    EU_DEG,
    EU_M,
    EU_KM,
    EU_AU,
    EU_LY,
    EU_PC,
    EU_KPC
}

public enum TimeUnit
{
    TU_H, 
    TU_D, 
    TU_YR, 
    TU_DEC, 
    TU_CY, 
    TU_MIL, 
    TU_GYR
}

public class OrbitalElement
{
    public OrbitalElementUnit elementUnit;
    public TimeUnit timeUnit;
    public double baseConstant;
    public double rateVariable;

    public double CalculateValue(double inputTime, TimeUnit inputTimeUnit = TimeUnit.TU_CY)
    {
        double value = 0.0, targetValue = 0.0, targetTime = 0.0;

        // convert between time units
        if(inputTimeUnit == TimeUnit.TU_CY)
        {
            if(timeUnit == TimeUnit.TU_CY)
                targetTime = inputTime;
            else if(timeUnit == TimeUnit.TU_GYR)
                targetTime = inputTime / 2250000;
            else if(timeUnit == TimeUnit.TU_MIL)
                targetTime = inputTime / 10;
            else if(timeUnit == TimeUnit.TU_DEC)
                targetTime = inputTime * 10;
            else if(timeUnit == TimeUnit.TU_YR)
                targetTime = inputTime * 100;
            else if(timeUnit == TimeUnit.TU_D)
                targetTime = inputTime * 36525.0;
            else if(timeUnit == TimeUnit.TU_H)
                targetTime = inputTime * 36525.0 * 23.934472;
            else if(timeUnit == TimeUnit.TU_H)
                targetTime = inputTime * 36525.0 * 23.934472;
        }

        value = baseConstant + rateVariable * targetTime;

        // do not convert when there is no unit
        if(elementUnit == OrbitalElementUnit.EU_NONE)
            targetValue = value;

        // convert between length units
        else if(elementUnit == OrbitalElementUnit.EU_M)
            targetValue = value;
        else if(elementUnit == OrbitalElementUnit.EU_KM)
            targetValue = value * 1000;
        else if(elementUnit == OrbitalElementUnit.EU_AU)
            targetValue = value * Body.M_PER_AU;
        else if(elementUnit == OrbitalElementUnit.EU_LY)
            targetValue = value * Body.AU_PER_LY * Body.M_PER_AU;
        else if(elementUnit == OrbitalElementUnit.EU_PC)
            targetValue = value * Body.LY_PER_PC * Body.AU_PER_LY * Body.M_PER_AU;
        else if(elementUnit == OrbitalElementUnit.EU_KPC)
            targetValue = value * Body.LY_PER_PC * Body.AU_PER_LY * Body.M_PER_AU * 1000;

        // convert between angle units
        else if(elementUnit == OrbitalElementUnit.EU_RAD)
            targetValue = value * Mathf.Rad2Deg;
        else if(elementUnit == OrbitalElementUnit.EU_DEG)
            targetValue = value;

        return targetValue;
    }
}

public class MeanAnomalyConstants
{
    public double b;
    public double c;
    public double s;
    public double f;
}

public class OrbitalElementsEntry
{
    public Body body;
    public string bodyName;
    public Body primary;
    public string primaryName;
    public float referenceEpoch;
    public OrbitalElement semiMajorAxis;
    public OrbitalElement eccentricity;
    public OrbitalElement inclination;
    public OrbitalElement meanLongitude;
    public OrbitalElement longitudeOfAscendingNode;
    public OrbitalElement longitudeOfPeriapsis;
    public OrbitalElement argumentOfPeriapsis;
    public OrbitalElement meanAnomaly;
    public MeanAnomalyConstants meanAnomalyConstants;
}

public class OrbitalElementsDatabase
{
    private static string databaseFileName = @"Assets/Resources/Data/planetary_orbital_elements.xml";
    private static Dictionary<int, OrbitalElementsEntry> database;
    private static bool finishedLoading = false;

    public static Dictionary<int, OrbitalElementsEntry> Database {
        get { return database; }
    }

    public static bool FinishedLoading {
        get { return finishedLoading; }
    }

    public static void InitializeDatabase()
    {
        if(database == null)
            database = new Dictionary<int, OrbitalElementsEntry>();

        // reutilized when parsing references and units
        var astronomicalEpochConstants = Enum.GetValues(typeof(AstronomicalEpoch));
        var orbitalElementUnitConstants = Enum.GetValues(typeof(OrbitalElementUnit));
        var timeUnitConstants = Enum.GetValues(typeof(TimeUnit));

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(databaseFileName);

        XmlNode root = xmlDocument.DocumentElement;
        XmlNodeList bodies = xmlDocument.GetElementsByTagName("body");

        int numOrbitalElementsEntries = 0;

        // iterate through all available body nodes
        foreach(XmlNode body in bodies)
        {
            string bodyName = body["name"].InnerText;
            string primaryName = body["primary"].InnerText;

            if(bodyName.Length == 0 || primaryName.Length == 0)
                continue;

            OrbitalElementsEntry orbitalElementsEntry = new OrbitalElementsEntry();

            orbitalElementsEntry.bodyName = bodyName;
            orbitalElementsEntry.primaryName = primaryName;

            //string astronomicalEpochString = body["references"]["epoch"].InnerText;
            //bool validEpoch = false;
            
            //// find the correct astronomical epoch in the enumeration
            //foreach(AstronomicalEpoch astronomicalEpoch in astronomicalEpochConstants)
            //{
            //    if(string.Compare(astronomicalEpochString, astronomicalEpoch.ToString(), true) == 0 || string.Compare("AE_" + astronomicalEpochString, astronomicalEpoch.ToString(), true) == 0)
            //    {
            //        orbitalElementsEntry.referenceEpoch = astronomicalEpoch;
            //        validEpoch = true;
            //        break;
            //    }
            //}

            //if(!validEpoch)
            //    Engine.DebugLog("OrbitalElementsDatabase.InitializeDatabase(): invalid reference epoch {0} in body \"{1}\"!",
            //        astronomicalEpochString, bodyName);

            XmlNode elements = body["elements"];
            foreach(XmlNode element in elements)
            {
                string elementName = element.Name;
                if(string.Compare(elementName, "#comment", true) == 0)
                    continue;

                // if basic orbital element
                if(string.Compare(elementName, "mean_anomaly_constants", true) != 0)
                {
                    OrbitalElement orbitalElement = new OrbitalElement();

                    // parse orbital element unit
                    string orbitalElementUnitString = "";
                    if(element["unit"] != null)
                    {
                        orbitalElementUnitString = element["unit"].InnerText;
                        bool validOrbitalElementUnit = false;

                        // find the correct orbital element unit in the enumeration
                        if(orbitalElementUnitString.Length > 0)
                        {
                            foreach(OrbitalElementUnit orbitalElementUnit in orbitalElementUnitConstants)
                            {
                                if(string.Compare(orbitalElementUnitString, orbitalElementUnit.ToString(), true) == 0 || string.Compare("EU_" + orbitalElementUnitString, orbitalElementUnit.ToString(), true) == 0)
                                {
                                    orbitalElement.elementUnit = orbitalElementUnit;
                                    validOrbitalElementUnit = true;
                                    break;
                                }
                            }
                        }

                        if(!validOrbitalElementUnit)
                            Engine.DebugLog("OrbitalElementsDatabase.InitializeDatabase(): invalid orbital element unit {0} of element \"{1}\" in body \"{2}\"!",
                                orbitalElementUnitString, elementName, bodyName);
                    }
                    else // no unit specified (i.e. ratios or the like)
                    {
                        orbitalElement.elementUnit = OrbitalElementUnit.EU_NONE;
                    }

                    // parse time unit
                    string timeUnitString = "";
                    if(element["time_unit"] != null)
                    {
                        timeUnitString = element["time_unit"].InnerText;
                        bool validTimeUnit = false;

                        // find the correct time unit in the enumeration
                        if(timeUnitString.Length > 0)
                        {
                            foreach(TimeUnit timeUnit in timeUnitConstants)
                            {
                                if(string.Compare(timeUnitString, timeUnit.ToString(), true) == 0 || string.Compare("TU_" + timeUnitString, timeUnit.ToString(), true) == 0)
                                {
                                    orbitalElement.timeUnit = timeUnit;
                                    validTimeUnit = true;
                                    break;
                                }
                            }
                        }

                        if(!validTimeUnit)
                            Engine.DebugLog("OrbitalElementsDatabase.InitializeDatabase(): invalid time unit {0} of element \"{1}\" in body \"{2}\"!",
                                timeUnitString, elementName, bodyName);
                    }
                    else // default time unit: century
                    {
                        orbitalElement.timeUnit = TimeUnit.TU_CY;
                    }

                    // parse base constant
                    string baseConstantString = "";
                    if(element["base"] != null)
                    {
                        baseConstantString = element["base"].InnerText;
                        if(baseConstantString.Length > 0)
                            orbitalElement.baseConstant = Double.Parse(baseConstantString);
                    }
                    else
                    {
                        orbitalElement.baseConstant = 0.0;
                    }

                    // parse rate variable
                    string rateVariableString = "";
                    if(element["rate"] != null)
                    {
                        rateVariableString = element["rate"].InnerText;
                        if(rateVariableString.Length > 0)
                            orbitalElement.rateVariable = Double.Parse(rateVariableString);
                    }
                    else
                    {
                        orbitalElement.rateVariable = 0.0;
                    }

                    // map to eligible orbital element in entry
                    switch(elementName)
                    {
                        case "semi_major_axis":
                            orbitalElementsEntry.semiMajorAxis = orbitalElement;
                            break;

                        case "eccentricity":
                            orbitalElementsEntry.eccentricity = orbitalElement;
                            break;

                        case "inclination":
                            orbitalElementsEntry.inclination = orbitalElement;
                            break;

                        case "mean_longitude":
                            orbitalElementsEntry.meanLongitude = orbitalElement;
                            break;

                        case "longitude_of_ascending_node":
                            orbitalElementsEntry.longitudeOfAscendingNode = orbitalElement;
                            break;

                        case "longitude_of_periapsis":
                            orbitalElementsEntry.longitudeOfPeriapsis = orbitalElement;
                            break;

                        case "argument_of_periapsis":
                            orbitalElementsEntry.argumentOfPeriapsis = orbitalElement;
                            break;

                        case "mean_anomaly":
                            orbitalElementsEntry.meanAnomaly = orbitalElement;
                            break;

                        default:
                            Engine.DebugLog("OrbitalElementsDatabase.InitializeDatabase(): unsupported element name {0} in body \"{1}\"!", 
                                elementName, bodyName);
                            break;
                    }
                }
                // if set of mean anomaly constants
                else // i.e. elementName equals "mean_anomaly_constants"
                {
                    MeanAnomalyConstants meanAnomalyConstants = new MeanAnomalyConstants();

                    meanAnomalyConstants.b = element["b"] != null && element["b"].InnerText.Length > 0 ? Double.Parse(element["b"].InnerText) : 0.0;
                    meanAnomalyConstants.c = element["c"] != null && element["c"].InnerText.Length > 0 ? Double.Parse(element["c"].InnerText) : 0.0;
                    meanAnomalyConstants.s = element["s"] != null && element["s"].InnerText.Length > 0 ? Double.Parse(element["s"].InnerText) : 0.0;
                    meanAnomalyConstants.f = element["f"] != null && element["f"].InnerText.Length > 0 ? Double.Parse(element["f"].InnerText) : 0.0;

                    orbitalElementsEntry.meanAnomalyConstants = meanAnomalyConstants;
                }
            }

            database.Add(numOrbitalElementsEntries, orbitalElementsEntry);
            numOrbitalElementsEntries++;
        }
    }

    public static OrbitalElementsEntry GetOrbitalElements(string bodyName)
    {
        foreach(KeyValuePair<int, OrbitalElementsEntry> entry in database)
        {
            if(string.Compare(entry.Value.bodyName, bodyName, true) == 0)
                return entry.Value;
        }

        return null;
    }

    public static void DumpEphemeris(double julianDate, string fileNameSuffix = "")
    {
        double centuriesSinceJ2000 = Engine.JulianDateToCenturiesSinceJ2000(julianDate);
        DateTime dateUtc = Engine.JulianDateToDateTime(julianDate);

        string fileName = string.Format(@"Assets/Resources/Data/planetary_ephemeris_{0}_utc.xml", dateUtc.ToString("yyyyMMdd_HHmmss"));
        if(fileNameSuffix.Length > 0)
            fileName += fileNameSuffix;

        XmlDocument xmlDocument = new XmlDocument();
        XmlDeclaration declarationNode = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
        xmlDocument.AppendChild(declarationNode);

        XmlNode rootNode = xmlDocument.CreateElement("planetary_ephemeris");
        xmlDocument.AppendChild(rootNode);

        XmlNode dateTimeNode = xmlDocument.CreateElement("date_time");
        rootNode.AppendChild(dateTimeNode);

        XmlNode centuriesSinceJ2000Node = xmlDocument.CreateElement("centuries_since_j2000");
        centuriesSinceJ2000Node.InnerText = centuriesSinceJ2000.ToString();
        XmlNode julianDateNode = xmlDocument.CreateElement("julian_date");
        julianDateNode.InnerText = julianDate.ToString();
        XmlNode dateUtcNode = xmlDocument.CreateElement("date_utc");
        dateUtcNode.InnerText = dateUtc.ToString("yyyy-MM-dd HH:mm:ss");

        dateTimeNode.AppendChild(centuriesSinceJ2000Node);
        dateTimeNode.AppendChild(julianDateNode);
        dateTimeNode.AppendChild(dateUtcNode);

        Ephemeris ephemeris = new Ephemeris();

        foreach(KeyValuePair<int, OrbitalElementsEntry> entry in database)
        {
            // we don't want to search for the actual Body objects here, 
            //   so we simply rely on the Body having itself referenced correctly during initialization
            if(entry.Value.body == null || entry.Value.primary == null)
                continue;

            Body body = entry.Value.body;

            // continue with the next body if this body's ephemeris could not be calculated
            if(body.CalculateEphemeris(ephemeris, centuriesSinceJ2000) == false)
                continue;

            XmlNode bodyNode = xmlDocument.CreateElement("body");

            {
                XmlNode nameNode = xmlDocument.CreateElement("name");
                nameNode.InnerText = entry.Value.bodyName;
                bodyNode.AppendChild(nameNode);
            }

            {
                XmlNode primaryNode = xmlDocument.CreateElement("primary");
                primaryNode.InnerText = entry.Value.primaryName;
                bodyNode.AppendChild(primaryNode);
            }

            {
                XmlNode referencesNode = xmlDocument.CreateElement("references");
                bodyNode.AppendChild(referencesNode);

                {
                    XmlNode epochNode = xmlDocument.CreateElement("epoch");
                    epochNode.InnerText = entry.Value.referenceEpoch.ToString().Remove(0, 3);
                    referencesNode.AppendChild(epochNode);
                }
            }

            {
                XmlNode elementsNode = xmlDocument.CreateElement("elements");
                bodyNode.AppendChild(elementsNode);

                // iterate through the fields of the Ephemeris class
                FieldInfo[] fields = typeof(Ephemeris).GetFields();
                foreach(var field in fields)
                {
                    object[] omitInDumpAttributes = field.GetCustomAttributes(typeof(OmitInDumpAttribute), true);
                    if(omitInDumpAttributes.Length > 0)
                        continue;

                    var element = field.GetValue(ephemeris);
                    XmlNode elementNode = xmlDocument.CreateElement(FormatXmlFieldName(field));
                    elementsNode.AppendChild(elementNode);

                    XmlNode elementValueNode = xmlDocument.CreateElement("value");
                    elementValueNode.InnerText = element.ToString();
                    elementNode.AppendChild(elementValueNode);

                    object[] unitAttributes = field.GetCustomAttributes(typeof(UnitAttribute), true);
                    if(unitAttributes.Length > 0)
                    {
                        UnitAttribute unitAttribute = (UnitAttribute)unitAttributes[0];
                        XmlNode elementUnitNode = xmlDocument.CreateElement("unit");
                        elementNode.AppendChild(elementUnitNode);

                        XmlNode elementUnitShortNameNode = xmlDocument.CreateElement("short_name");
                        elementUnitShortNameNode.InnerText = unitAttribute.GetUnitString(UnitStringFormat.SHORT_UNIT_STRING_FORMAT);
                        elementUnitNode.AppendChild(elementUnitShortNameNode);

                        XmlNode elementUnitLongNameNode = xmlDocument.CreateElement("long_name");
                        elementUnitLongNameNode.InnerText = unitAttribute.GetUnitString(UnitStringFormat.LONG_UNIT_STRING_FORMAT);
                        elementUnitNode.AppendChild(elementUnitLongNameNode);
                    }
                }
            }

            rootNode.AppendChild(bodyNode);
        }

        xmlDocument.Save(fileName);
    }

    public static string FormatXmlFieldName(FieldInfo field)
    {
        string originalFieldName = field.ToString();

        // removes field type from field name
        string removePattern = @"([\S]* )";
        string removeReplace = @"";
        Regex removeRegExp = new Regex(removePattern);
        string formattedFieldName = removeRegExp.Replace(originalFieldName, removeReplace);

        // places an underscore before each capital letter
        string pattern = @"([A-Z])";
        string replace = @"_$1";
        Regex regExp = new Regex(pattern);
        formattedFieldName = regExp.Replace(formattedFieldName, replace).ToLower();

        return formattedFieldName;
    }
}
