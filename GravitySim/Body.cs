﻿/*
 * Author: Sebastian Zander
 *
 * The central game object class for a personal 3D gravity and 
 * solar system simulation. The later version of the simulation
 * concentrated on correctly illustrating the solar system based 
 * on one of the later models and orbital elements as published 
 * by Jet Propulsion Laboratories (JPL)
 *
 * Unbound simulation mode however allowed the planets to move
 * and follow trajectories based on physical laws and also 
 * extrapolated orbital elements and values at any given moment
 * in time.
 */

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

#pragma warning disable 0219
#pragma warning disable 0414

public class Ephemeris
{
    // orbital characteristics
    [Unit(UnitType.m)]
    public double semiMajorAxis;
    [Unit(UnitType.m)]
    public double periapsis;
    [Unit(UnitType.m)]
    public double apoapsis;
    [Unit(UnitType.m)]
    public double orbitalRadius;
    public double eccentricity;
    [Unit(UnitType.J_per_kg)]
    public double orbitalEnergy;
    [Unit(UnitType.sec)]
    public double orbitalPeriod;
    [OmitInDump]
    public string orbitalPeriodString;
    [OmitInDump]
    private double orbitalPeriodRealtime;
    [Unit(UnitType.deg)]
    public double meanMotion;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double inclination;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double meanLongitude;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double longitudeOfPeriapsis;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double longitudeOfAscendingNode;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double argumentOfPeriapsis;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double meanAnomaly;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double eccentricAnomaly;
    [Unit(UnitType.deg), Range(-360f, 360f)]
    public double trueAnomaly;
    [Unit(UnitType.jd)]
    public double timeSincePeriapsis;
    [Unit(UnitType.jd)]
    public double timeOfPeriapsis;

    // physical characteristics
    [OmitInDump]
    public double obliquity;
}

[AddComponentMenu("GravitySim/Body")]
public class Body : MonoBehaviour
{
    private static Dictionary<ulong, Body> bodies = new Dictionary<ulong, Body>();

    public const float G = 0.0000000000667408f; // gravitational constant
    public const float KM_PER_AU = 149597870.7f;
    public const float M_PER_AU = KM_PER_AU * 1000f;
    public const float KM_PER_GU = 1.0e4f; // kilometers per game unit
    public const float M_PER_GU = KM_PER_GU * 1000f; // meters per game unit
    public const float AU_PER_GU = KM_PER_GU / KM_PER_AU; // astronomical units per game unit
    public const float AU_PER_LY = 63241.1f; // astronomical units per light year
    public const float LY_PER_PC = 3.26156f; // light years per parsec

    public const float KG_PER_GU = 1.0e24f; // kilograms per game unit
    public const float S_PER_DAY = 86400; // seconds per (Julian) day
    public const float S_PER_CY = S_PER_DAY * 365.25f * 100f;

    private const int MIN_POSITION_UPDATES_PER_CYCLE = 180; // minimum number of position updates within an orbital cycle
    private const int MAX_POSITION_UPDATES_PER_FRAME = 16;
    private const int SWITCH_PREPARE_OFFSET = 1000;

    private const float TRAIL_TIME = 500f; // 500 seconds life span in realtime
    private const float TRAIL_MIN_TIME = 1f; // 5 seconds minimum life span
    private const float TRAIL_MAX_TIME = 1200f; // 20 minutes maximum life span
    private const float TRAIL_MIN_WIDTH = 20f;
    private const float TRAIL_WIDTH_DIVIDER = 1000f;
    private const float TRAIL_WIDTH_ADJUST_MAX_CAMERA_DISTANCE = 200000f;
    private const float TRAIL_SCREEN_WIDTH = 2f;
    private const float TRAIL_COLOR_ALPHA = 0.45f;
    private const float ORBIT_LINE_WIDTH_DIVIDER = 1000f;
    private const float ORBIT_LINE_SCREEN_WIDTH = 1f;
    private const float ICON_SCALE_DIVIDER = 200f;

    public const int RENDER_LABEL_OFF = 0;
    public const int RENDER_LABEL_ON = 1;
    public const int RENDER_LABEL_INVISIBLE = 2;

    public const int RENDER_TRAIL_OFF = 0;
    public const int RENDER_TRAIL_ON = 1;
    public const int RENDER_TRAIL_ALLWHITE = 2;

    // General attributes
    [Header("General attributes")]
    public bool isBarycenter = false;
    public Color color;
    private string hexColor;
    [Range(1f, 50f)]
    public float brightness = 1f;
    private static GameObject labelPrefab;
    private GameObject label;
    public Text bodyLabel;
    public bool initUsingKeplerianElements = false;
    public double timeAlive = 0.0;
    public double timeAliveRealtime = 0.0;

    // Physical characteristics
    [Header("Physical characteristics")]
    public float mass;
    public float radius;
    [Tooltip("Angle between the body's equatorial plane and orbital plane [deg].")]
    [Range(0f, 360f)]
    public float axialTilt;
    [Tooltip("The time that it takes to complete one revolution around the body's axis of rotation relative to the background stars [days].")]
    public float siderialRotationPeriod;
    [Tooltip("Daily progress of the rotation in respect to the international date line [decimal; 0-1].")]
    public float rotationProgress;
    [Tooltip("Total progress of the rotation/progress of the days in respect to the international date line [decimal].")]
    public double rotationPeriod;
    [Tooltip("Current rotation angle of the body as the isolated Y-component of the transform's rotation [deg].")]
    public float rotationAngle;
    [Tooltip("Current longitude of noon where the sun/main star is at its highest in the sky [deg].")]
    public float noonLongitude;
    public Texture2D elevationMap;
    [Range(0f, 1f)]
    public float elevationMapPrimeMeridianPosition;
    public float minElevation;
    public float maxElevation;
    public float minGrayscaleValue = 0f;
    public float maxGrayscaleValue = 1f;

    // Orbital characteristics
    [Header("Orbital characteristics")]
    [Tooltip("Orbital inclination of the body above the invariable plane [deg].")]
    public Body primary;
    public bool boundOrbit = false;
    public double semiMajorAxis;
    public double periapsis;
    public double apoapsis;
    public double orbitalRadius;
    public double eccentricity;
    public double orbitalEnergy;
    public double orbitalPeriod;
    public string orbitalPeriodString;
    [Range(-360f, 360f)]
    public double inclination;
    [Range(-360f, 360f)]
    public double longitudeOfAscendingNode;
    [Range(-360f, 360f)]
    public double argumentOfPeriapsis;
    [Range(0f, 360f)]
    public double meanAnomaly;
    [Range(0f, 360f)]
    public double eccentricAnomaly;
    [Range(0f, 360f)]
    public double trueAnomaly;
    public double meanMotion;

    // Orbital measurements
    [Header("Orbital measurements")]
    public double semiMajorAxisMeasured;
    public double periapsisMeasured;
    public double apoapsisMeasured;
    public double orbitalRadiusMeasured;
    public double eccentricityMeasured;
    public double orbitalEnergyMeasured;
    public double orbitalPeriodMeasured;
        public string orbitalPeriodMeasuredString;
        private double orbitalPeriodRealtimeMeasured;
    public double orbitalSpeed;
    public double orbitalSpeedKMS;
    public double orbitalCycleProgress = 0f;
    public int orbitalCyclesCompleted = 0;
        private float timeSinceLastCompletedOrbitalCycle = 0f;
    [Range(-360f, 360f)]
    public double inclinationMeasured = 0f;
    [Range(-360f, 360f)]
    public double longitudeOfAscendingNodeMeasured = 0f;
    [Range(-360f, 360f)]
    public double argumentOfPeriapsisMeasured = 0f;
    [Range(0f, 360f)]
    public double meanAnomalyMeasured = 0f;
    [Range(0f, 360f)]
    public double eccentricAnomalyMeasured = 0f;
    [Range(0f, 360f)]
    public double trueAnomalyMeasured = 0f;
    public double meanMotionMeasured = 0f;
    public double secondsSincePeriapsis;
    public double timeOfPeriapsis;
    private DateTime dateOfPeriapsis;
    public string dateOfPeriapsisString;
    public double axialTiltMeasured = 0f;

    // Atmosphere characteristics
    [Header("Atmosphere characteristics")]
    public float cloudLayerSiderealRotationCoeffient = 0.9f;

    // Debug attributes
    // only meant to be public in development phase
    [Header("Debug attributess")]
    public bool enableDebugOutput = false;
    public bool enableStationaryOrbitCam = false;
    public string orbitalElementsBodyName = "";
    public bool useOrbitalElementsDatabase = true;
    public Camera stationaryOrbitCam;
    public float relativeIconScale = 0.7f;
    public double orbitalPeriodRealtime;
    public float scaledTrailTime;
    public int positionUpdatesPerFrame;
    public float updatesPerOrbit;

    private ulong id;
    private string lastName = "";

    // physics
    private Vector3 truePosition;
    private Vector3 velocity;
    private Vector3 trueVelocity;
    private Vector3 momentum;
    private Vector3 eccentricityVector;
    private Vector3 n;
    private Vector3 acceleration;
    private Vector3 attraction;
    private double julianDate;
    private double lastJulianDate = -1;
    private Ephemeris ephemeris;
    private OrbitalElementsEntry orbitalElementsEntry;
    private List<Body> satellites = new List<Body>();

    // components
    private Rigidbody rigidbodyComponent;
    private MeshRenderer meshRenderer;
    private TrailRenderer trail;
    private int clearTrailsOnNextPositionUpdate = 0;
    private LensFlare lensFlare;
    private StarSystem system;
    private GameObject icon;
    private Color iconColor;
    private Terminator terminator;
    private List<Transform> cloudLayers = new List<Transform>();
    private Text distancesNamesLabel;
    private Text distancesInfoLabel;

    private Vector3 mutualForceVector = new Vector3();
    private float cameraDistance;

    private int renderMesh = 1;
    private int renderIcon = 1;
    private int renderLensFlare = 1;
    private int renderTrail = 1;
    private int renderLabel = RENDER_LABEL_ON;

    private long updateNumber = 0L;


    /* PROPERTIES */

    public ulong Id {
        get { return id; }
    }

    public string HexColor {
        get { return hexColor; }
    }

    public int RenderMesh {
        get { return renderMesh; }
        set { renderMesh = Mathf.Clamp(value, 0, 1); }
    }

    public int RenderIcon {
        get { return renderIcon; }
        set { renderIcon = Mathf.Clamp(value, 0, 1); }
    }

    public int RenderLensFlare {
        get { return renderLensFlare; }
        set { renderLensFlare = Mathf.Clamp(value, 0, 1); }
    }

    public int RenderTrail {
        get { return renderTrail; }
        set {
            int old = renderTrail;
            renderTrail = PrepareSwitch(Mathf.Clamp(value, 0, 1));
            if(old == 0 && renderTrail == PrepareSwitch(1) && trail != null)
                trail.Clear();
        }
    }

    public int RenderLabel {
        get { return renderLabel; }
        set { renderLabel = PrepareSwitch(Mathf.Clamp(value, 0, 1)); }
    }

    public string NameRichtext
    {
        get { return string.Format("<color={0}>{1}</color>", hexColor, name); }
    }

    // Use this for initialization
    void Start()
    {
        UpdateId();

        // call to ensure positions are correct in respect to simulation date and time
        OnValidate();

        hexColor = "#ffffff"; // default body hex color

        if(labelPrefab == null)
        {
            labelPrefab = Resources.Load("Prefabs/BodyInfoLabel") as GameObject;
        }

        if(bodyLabel == null)
        {
            label = Instantiate(labelPrefab);
            label.name = string.Format("{0} Info Label", this.name);
            label.transform.SetParent(GameObject.Find("/GUI Canvas/Body Labels").transform, false);
            bodyLabel = label.GetComponent<Text>();
        }

        bodyLabel.GetComponent<ObjectLabel>().target = this.transform;
        bodyLabel.color = color;
        bodyLabel.text = name.ToUpper();

        // determine body hex color for console output
        if(color.r + color.g + color.b >= 0.3f)
        {
            int r = (int)(color.r * 255);
            int g = (int)(color.g * 255);
            int b = (int)(color.b * 255);

            hexColor = string.Format("#{0}{1}{2}", r.ToString("X2"), g.ToString("X2"), b.ToString("X2"));
        }

        rigidbodyComponent = GetComponent<Rigidbody>();
        if(rigidbodyComponent != null)
        {
            rigidbodyComponent.mass = mass / KG_PER_GU;
        }

        /* initialize attributes */

        julianDate = Engine.JulianDateInSimulation;
        rotationAngle = 0f;

        if(orbitalElementsBodyName.Length == 0)
            orbitalElementsBodyName = name;
        orbitalElementsEntry = OrbitalElementsDatabase.GetOrbitalElements(orbitalElementsBodyName);

        if(orbitalElementsEntry != null)
        {
            orbitalElementsEntry.body = this;
            orbitalElementsEntry.primary = primary;
        }
        //else
        //    Engine.DebugLog("Body.Start(): could not get orbital elements for body name \"{0}\"", orbitalElementsBodyName);

        ephemeris = new Ephemeris();

        //if(enableDebugOutput)
        //    Engine.DebugLog("{0}.Start(): julian date at start: {1:0.00000} JD", NameRichtext, julianDate);

        UpdateOrbitalCharacteristics(julianDate);
        UpdatePosition();
        UpdateVelocity();
        UpdateRotation();

        cameraDistance = Vector3.Distance(Camera.main.GetComponent<Transform>().position, transform.position);

        // set mesh renderer
        meshRenderer = GetComponent<MeshRenderer>();

        // set trail width based on distance to main camera
        trail = GetComponent<TrailRenderer>();
        if(trail != null && trail.enabled)
        {
            // the following is necessary because of the OnValidate() call at the beginning of Start()
            // there would otherwise be a magic move/jump of the body visible in its trail
            trail.Clear();

            float trailWidth = (cameraDistance > TRAIL_WIDTH_ADJUST_MAX_CAMERA_DISTANCE ? TRAIL_WIDTH_ADJUST_MAX_CAMERA_DISTANCE : cameraDistance) /
                TRAIL_WIDTH_DIVIDER * TRAIL_SCREEN_WIDTH;

            trail.startWidth = trail.endWidth = trailWidth;

            scaledTrailTime = TRAIL_TIME / Engine.TimeScale;
            if(primary != null)
                scaledTrailTime = (float)(orbitalPeriodRealtime * (1 - eccentricity * 0.8));
            scaledTrailTime = Mathf.Clamp(scaledTrailTime, TRAIL_MIN_TIME, TRAIL_MAX_TIME);

            trail.time = scaledTrailTime;

            Gradient trailGradient = new Gradient();
            GradientColorKey[] trailGradientColors;
            GradientAlphaKey[] trailGradientAlphas;
            trailGradientColors = new GradientColorKey[2];
            trailGradientAlphas = new GradientAlphaKey[2];
            if(renderTrail == RENDER_TRAIL_ALLWHITE)
            {
                trailGradientColors[0].color = Color.white;
                trailGradientColors[1].color = Color.white;
                trailGradientAlphas[0].alpha = TRAIL_COLOR_ALPHA * 0.6f;
            }
            else
            {
                trailGradientColors[0].color = color;
                trailGradientColors[1].color = color;
                trailGradientAlphas[0].alpha = TRAIL_COLOR_ALPHA;
            }
            trailGradientColors[0].time = 0.0f;
            trailGradientColors[1].time = 1.0f;
            trailGradientAlphas[0].time = 0.0f;
            trailGradientAlphas[1].alpha = 0.0f;
            trailGradientAlphas[1].time = 1.0f;
            trailGradient.SetKeys(trailGradientColors, trailGradientAlphas);
            trail.colorGradient = trailGradient;
        }

        // set lens flare brightness based on distance to main camera
        lensFlare = GetComponent<LensFlare>();
        if(lensFlare != null)
        {
            lensFlare.brightness = 40f / Mathf.Sqrt(cameraDistance) + 0.5f;
            lensFlare.enabled = true;
        }

        // register body at its primary as a satellite
        /*if(primary != null)
        {
            primary.RegisterSatellite(this);
        }
        // register in the next higher star system otherwise
        else*/
        {
            system = GetComponentInParent<StarSystem>();
            if(system != null)
                system.RegisterSatellite(this);
        }

        icon = transform.Find("Icon").gameObject;
        if(icon != null)
        {
            float h, s, v;
            h = s = v = 0f;
            Color.RGBToHSV(color, out h, out s, out v);
            s *= 0.5f;
            iconColor = Color.HSVToRGB(h, s, v);
            icon.GetComponent<Renderer>().material.color = iconColor;
            icon.GetComponent<Renderer>().material.SetColor("_EmissionColor", iconColor * brightness);

            icon.SetActive(true);
        }

        terminator = GetComponentInChildren<Terminator>();

        //cloudLayers = new List<Transform>();
        for(int i = 1; i <= 3; i++) {
            Transform cloudLayer = transform.Find(string.Format("CloudLayer{0}", i));
            if(cloudLayer != null)
                cloudLayers.Add(cloudLayer);
        }

        distancesNamesLabel = GameObject.Find("Distances Names Label").GetComponent<Text>();
        distancesInfoLabel = GameObject.Find("Distances Info Label").GetComponent<Text>();

        /* initialize orbital measurements */

        InitializeOrbitalMeasurements();
    }

    void OnEnable()
    {
        if(trail != null)
            trail.enabled = true;
        if(bodyLabel != null)
            bodyLabel.enabled = true;
    }

    void OnDisable()
    {
        if(trail != null)
            trail.enabled = false;
        if(bodyLabel != null)
            bodyLabel.enabled = false;
    }

    // Called when a property value is changed in the inspector
    void OnValidate()
    {
        julianDate = Engine.JulianDateAtProgramStart;

        UpdatePhysicalCharacteristics();
        UpdateOrbitalCharacteristics(julianDate);

        if(!Application.isPlaying)
        {
            /* validate property values */

            // determine body hex color for console output
            if(color.r + color.g + color.b >= 0.3f)
            {
                int r = (int)(color.r * 255);
                int g = (int)(color.g * 255);
                int b = (int)(color.b * 255);

                hexColor = string.Format("#{0}{1}{2}", r.ToString("X2"), g.ToString("X2"), b.ToString("X2"));
            }
            else
            {
                hexColor = "#ffffff";
            }

            terminator = GetComponentInChildren<Terminator>();

            UpdatePosition();
            UpdateVelocity();
            UpdateRotation(true);

            if(!initUsingKeplerianElements)
            {
                UpdateOrbitalMeasurements();
                UpdateOrbitalEnergy();
                UpdateOrbitalPeriod();
                UpdateAnomaly();
            }

            orbitalCycleProgress = 0f;
            orbitalCyclesCompleted = 0;

            if(bodyLabel != null)
            {
                bodyLabel.color = color;
                bodyLabel.text = name;
            }
        }

        if(stationaryOrbitCam != null)
        {
            if(enableStationaryOrbitCam && stationaryOrbitCam.enabled == false)
            {
                stationaryOrbitCam.enabled = true;
            }
            else if(!enableStationaryOrbitCam && stationaryOrbitCam.enabled == true)
            {
                stationaryOrbitCam.enabled = false;
            }
        }

        if(Application.isPlaying)
            UpdateId();
    }

    private void UpdateId()
    {
        // only update id if the body's name has changed
        if(!lastName.Equals(name))
        {
            ulong newId = StringUtilities.GetIntegerHash(name);
            if(newId != id)
            {
                if(id != 0)
                    bodies.Remove(id);
                id = newId;
                bodies.Add(id, this);
            }

            lastName = name;
        }
    }

    public static Body GetBodyById(ulong id)
    {
        if(bodies.ContainsKey(id))
            return bodies[id];
        return null;
    }

    public static Body GetBodyByName(string name)
    {
        foreach(Body body in bodies.Values)
            if(body.name.Equals(name))
                return body;
        return null;
    }

    public bool CalculateEphemeris(Ephemeris ephemeris, double centuriesSinceJ2000)
    {
        if(primary == null || ephemeris == null /*|| primary.name != "Sun"*/)
            return false;

        if(useOrbitalElementsDatabase && orbitalElementsEntry != null)
        {
            //AstronomicalEpoch referenceEpoch = orbitalElementsEntry.referenceEpoch;
            double referenceTime = 0.0;
            //referenceTime = centuriesSinceJ2000;

            if(name == "Moon")
            {
                referenceTime = 1.0;
            }

            ephemeris.semiMajorAxis = orbitalElementsEntry.semiMajorAxis.CalculateValue(centuriesSinceJ2000);
            ephemeris.eccentricity = orbitalElementsEntry.eccentricity.CalculateValue(centuriesSinceJ2000);
            ephemeris.periapsis = ephemeris.semiMajorAxis * (1 - ephemeris.eccentricity);
            ephemeris.apoapsis = ephemeris.semiMajorAxis * (1 + ephemeris.eccentricity);
            ephemeris.orbitalEnergy = -(G * primary.mass) / 2 * ephemeris.semiMajorAxis;
            ephemeris.orbitalPeriod = 2 * Math.PI * Math.Sqrt(Math.Pow(ephemeris.semiMajorAxis, 3) / (G * (primary.mass + this.mass)));
            ephemeris.orbitalPeriodString = Engine.TimeToString(ephemeris.orbitalPeriod, 1.1, "0.0000");
            ephemeris.meanMotion = 360 / ephemeris.orbitalPeriod;
            ephemeris.inclination = orbitalElementsEntry.inclination.CalculateValue(centuriesSinceJ2000);

            if(orbitalElementsEntry.meanLongitude != null)
                ephemeris.meanLongitude = orbitalElementsEntry.meanLongitude.CalculateValue(centuriesSinceJ2000);

            if(orbitalElementsEntry.longitudeOfPeriapsis != null)
                ephemeris.longitudeOfPeriapsis = orbitalElementsEntry.longitudeOfPeriapsis.CalculateValue(centuriesSinceJ2000);

            ephemeris.longitudeOfAscendingNode = orbitalElementsEntry.longitudeOfAscendingNode.CalculateValue(centuriesSinceJ2000);

            if(orbitalElementsEntry.argumentOfPeriapsis != null)
                ephemeris.argumentOfPeriapsis = orbitalElementsEntry.argumentOfPeriapsis.CalculateValue(centuriesSinceJ2000);
            else
                ephemeris.argumentOfPeriapsis = ephemeris.longitudeOfPeriapsis - ephemeris.longitudeOfAscendingNode;

            double meanAnomaly = 0.0;
            if(orbitalElementsEntry.meanAnomaly != null)
                meanAnomaly = orbitalElementsEntry.meanAnomaly.CalculateValue(centuriesSinceJ2000);
            else
                meanAnomaly = ephemeris.meanLongitude - ephemeris.longitudeOfPeriapsis;

            if(orbitalElementsEntry.meanAnomalyConstants != null)
            {
                MeanAnomalyConstants mac = orbitalElementsEntry.meanAnomalyConstants;
                meanAnomaly += mac.b * centuriesSinceJ2000 * centuriesSinceJ2000 +
                    mac.c * Math.Cos(mac.f * centuriesSinceJ2000) + mac.s * Math.Sin(mac.f * centuriesSinceJ2000);
            }

            ephemeris.meanAnomaly = MathUtilities.NormalizeAngle(meanAnomaly);

            ephemeris.eccentricAnomaly = AstroUtilities.CalculateEccentricAnomaly(ephemeris.meanAnomaly, ephemeris.eccentricity);
            ephemeris.trueAnomaly = AstroUtilities.CalculateTrueAnomaly(ephemeris.eccentricAnomaly, ephemeris.eccentricity);
            ephemeris.orbitalRadius = ephemeris.semiMajorAxis * (1 - ephemeris.eccentricity * ephemeris.eccentricity) / (1 + ephemeris.eccentricity * Math.Cos(ephemeris.trueAnomaly * Mathf.Deg2Rad));
            ephemeris.timeSincePeriapsis = ephemeris.meanAnomaly / 360 * ephemeris.orbitalPeriod;
            ephemeris.timeOfPeriapsis = Engine.JulianDateAtProgramStart - ephemeris.timeSincePeriapsis / S_PER_DAY;

            // not yet implemented into orbital elements database
            ephemeris.obliquity = 0.0;
        }

        return true;
    }

    // Registers a body to the list of satellites
    public void RegisterSatellite(Body body)
    {
        if(body != null && !satellites.Contains(body))
        {
            satellites.Add(body);
            //Engine.DebugLog("Body.RegisterSatellite(): registered {0} as satellite of {1}", body.NameRichtext, NameRichtext);
        }
    }

    // Unregisters a body from the list of satellites
    public void UnregisterSatellite(Body body)
    {
        if(body != null && satellites.Contains(body))
        {
            satellites.Remove(body);
        }
    }

    public void UpdateSatellites()
    {
        foreach(Body a in satellites)
        {
            UpdateBody();
        }
    }

    private void FixedUpdate()
    {
        UpdateBody();
    }

    public void UpdateBody()
    {
        updateNumber++;

        timeAlive += Time.fixedDeltaTime * Engine.TimeScale;
        timeAliveRealtime += Time.fixedDeltaTime;

        if(rigidbodyComponent != null)
        {
            rigidbodyComponent.mass = mass / KG_PER_GU;
        }

        cameraDistance = Vector3.Distance(Camera.main.GetComponent<Transform>().position, transform.position);

        // set mesh
        if(meshRenderer != null)
        {
            // enable/disable stuff
            if(meshRenderer.enabled && (/*renderMeshAll == 0 ||*/ renderMesh == 0))
                meshRenderer.enabled = false;
            else if(!meshRenderer.enabled && (/*renderMeshAll == 1 &&*/ renderMesh == 1))
                meshRenderer.enabled = true;
        }

        // set body icon
        if(icon != null && name != "Sun")
        {
            // enable/disable stuff
            if(icon.activeSelf && (/*renderIconAll == 0 ||*/ renderIcon == 0))
                icon.SetActive(false);
            else if(!icon.activeSelf && (/*renderIconAll == 1 &&*/ renderIcon == 1))
                icon.SetActive(true);

            float iconScale = cameraDistance / ICON_SCALE_DIVIDER * relativeIconScale;
            icon.transform.localScale = new Vector3(iconScale / transform.localScale.x, iconScale / transform.localScale.y, iconScale / transform.localScale.z);
        }

        // set lens flare
        if(lensFlare != null)
        {
            // enable/disable stuff
            if(lensFlare.enabled && (/*renderLensFlareAll == 0 ||*/ renderLensFlare == 0))
                lensFlare.enabled = false;
            else if(!lensFlare.enabled && (/*renderLensFlareAll == 1 &&*/ renderLensFlare == 1))
                lensFlare.enabled = true;

            lensFlare.brightness = 40f / Mathf.Sqrt(cameraDistance) + 0.05f;
        }

        if(label != null)
        {
            if(renderLabel == PrepareSwitch(RENDER_LABEL_OFF) || renderLabel == PrepareSwitch(RENDER_LABEL_INVISIBLE))
            {
                label.SetActive(false);
                renderLabel -= SWITCH_PREPARE_OFFSET;
            }
            else if(renderLabel == PrepareSwitch(RENDER_LABEL_ON))
            {
                label.SetActive(true);
                renderLabel -= SWITCH_PREPARE_OFFSET;
            }
        }

        // update trail width based on distance to main camera
        if(trail != null)
        {
            if(renderTrail == SWITCH_PREPARE_OFFSET + RENDER_TRAIL_OFF)
            {
                trail.enabled = false;
                renderTrail -= SWITCH_PREPARE_OFFSET;
            }
            else if(renderTrail == SWITCH_PREPARE_OFFSET + RENDER_TRAIL_ON)
            {
                trail.enabled = true;
                renderTrail -= SWITCH_PREPARE_OFFSET;
            }

            float trailWidth = (cameraDistance > TRAIL_WIDTH_ADJUST_MAX_CAMERA_DISTANCE ? TRAIL_WIDTH_ADJUST_MAX_CAMERA_DISTANCE : cameraDistance) /
                TRAIL_WIDTH_DIVIDER * TRAIL_SCREEN_WIDTH;

            trail.startWidth = trail.endWidth = trailWidth;

            scaledTrailTime = TRAIL_TIME / Engine.TimeScale;
            if(primary != null)
            {
                scaledTrailTime = (float)(orbitalPeriodRealtimeMeasured > 0 ? orbitalPeriodRealtimeMeasured : orbitalPeriodRealtime);
                scaledTrailTime *= (float)(1 - eccentricityMeasured * 0.8);
            }

            scaledTrailTime = Mathf.Clamp(scaledTrailTime, TRAIL_MIN_TIME, TRAIL_MAX_TIME);

            if(trail.time != scaledTrailTime)
            {
                trail.time = scaledTrailTime;
            }
        }

        julianDate = Engine.JulianDateInSimulation;

        if(boundOrbit)
        {
            double deltaJulianDays = lastJulianDate > 0.0 ? julianDate - lastJulianDate : 1.0;
            double deltaSeconds = deltaJulianDays * Engine.SECONDS_PER_DAY;

            if(orbitalPeriod > 0f)
            {
                updatesPerOrbit = (float)(orbitalPeriod / deltaSeconds);
                positionUpdatesPerFrame = Mathf.Clamp(Mathf.CeilToInt(MIN_POSITION_UPDATES_PER_CYCLE / updatesPerOrbit), 1, MAX_POSITION_UPDATES_PER_FRAME);
            }
            else
            {
                updatesPerOrbit = 180f;
                positionUpdatesPerFrame = 1;
            }

            if(positionUpdatesPerFrame > 1)
            {
                double intermediateJulianDateStep = deltaJulianDays / positionUpdatesPerFrame;
                for(int i = 1; i < positionUpdatesPerFrame; i++)
                {
                    double intermediateJulianDate = lastJulianDate + intermediateJulianDateStep * i;
                    UpdateOrbitalCharacteristics(intermediateJulianDate);
                    UpdatePosition();
                    UpdateVelocity();
                }
            }

            UpdateOrbitalCharacteristics(julianDate);
            UpdatePosition();
            UpdateVelocity();

            lastJulianDate = julianDate;
        }

        if(clearTrailsOnNextPositionUpdate > 0 && trail != null)
        {
            trail.Clear();
            clearTrailsOnNextPositionUpdate--;
        }

        UpdateRotation();

        /* update orbital characteristics */

        UpdateOrbitalMeasurements();
        UpdateOrbitalEnergy();
        UpdateOrbitalPeriod();
        UpdateAnomaly();
        UpdateDates();

        //UpdateSatellites();
    }

    void Update()
    {
        UpdateInputs();
        UpdateLabels();
    }

    private void UpdatePhysicalCharacteristics()
    {
        if(radius > 0f)
        {
            // update body scale based on radius
            float scale = radius / M_PER_GU;
            //if(name == "Earth" || name == "Mars")
            //    scale *= 100; // debug only
            transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    private void UpdateOrbitalCharacteristics(double julianDay)
    {
        double centuriesSinceJ2000 = Engine.JulianDateToCenturiesSinceJ2000(julianDay);
        bool validEphemeris = CalculateEphemeris(ephemeris, centuriesSinceJ2000);

        if(initUsingKeplerianElements && validEphemeris)
        {
            semiMajorAxis = ephemeris.semiMajorAxis;
            periapsis = ephemeris.periapsis;
            apoapsis = ephemeris.apoapsis;
            orbitalRadius = ephemeris.orbitalRadius;
            eccentricity = ephemeris.eccentricity;
            inclination = ephemeris.inclination;
            longitudeOfAscendingNode = ephemeris.longitudeOfAscendingNode;
            argumentOfPeriapsis = ephemeris.argumentOfPeriapsis;
            meanAnomaly = ephemeris.meanAnomaly;
            eccentricAnomaly = ephemeris.eccentricAnomaly;
            trueAnomaly = ephemeris.trueAnomaly;
            meanMotion = ephemeris.meanMotion;

            //axialTilt = ephemeris.obliquity;
        }
        else
        {
            eccentricity = Math.Max(eccentricity, 0f);
            semiMajorAxis = Math.Max(semiMajorAxis, 0f);
            periapsis = semiMajorAxis * (1 - eccentricity);
            apoapsis = semiMajorAxis * (1 + eccentricity);
        }
    }

    private void UpdatePosition()
    {
        if(primary != null)
            transform.position = CalculateOrbitalPosition(this.eccentricAnomaly);
    }

    public Vector3 CalculateOrbitalPosition(double eccentricAnomaly)
    {
        if(primary == null)
            return Vector3.zero;

        /* update body position based on orbital characteristics */

        double newLocalPositionX = 0f, newLocalPositionY = 0f, newLocalPositionZ = 0f;

        // generate coordinate system
        double p = semiMajorAxis * MathUtilities.Dcos(eccentricAnomaly) - eccentricity;
        double q = semiMajorAxis * MathUtilities.Dsin(eccentricAnomaly) * Math.Sqrt(1 - Math.Pow(eccentricity, 2));

        // rotate by argument of periapsis
        newLocalPositionX = MathUtilities.Dcos(argumentOfPeriapsis) * p - MathUtilities.Dsin(argumentOfPeriapsis) * q;
        newLocalPositionZ = MathUtilities.Dsin(argumentOfPeriapsis) * p + MathUtilities.Dcos(argumentOfPeriapsis) * q;

        // rotate by inclination
        newLocalPositionY = MathUtilities.Dsin(inclination) * newLocalPositionX;
        newLocalPositionZ = MathUtilities.Dcos(inclination) * newLocalPositionX;

        // rotate by longitude of ascending node
        double x = newLocalPositionX, z = newLocalPositionZ;
        newLocalPositionX = MathUtilities.Dcos(longitudeOfAscendingNode) * x - MathUtilities.Dsin(longitudeOfAscendingNode) * z;
        newLocalPositionZ = MathUtilities.Dsin(longitudeOfAscendingNode) * x + MathUtilities.Dcos(longitudeOfAscendingNode) * z;

        // JPL method:
        newLocalPositionX = (MathUtilities.Dcos(argumentOfPeriapsis ) * MathUtilities.Dcos(longitudeOfAscendingNode ) - MathUtilities.Dsin(argumentOfPeriapsis ) * MathUtilities.Dsin(longitudeOfAscendingNode ) * MathUtilities.Dcos(inclination )) * p +
            (-MathUtilities.Dsin(argumentOfPeriapsis ) * MathUtilities.Dcos(longitudeOfAscendingNode ) - MathUtilities.Dcos(argumentOfPeriapsis ) * MathUtilities.Dsin(longitudeOfAscendingNode ) * MathUtilities.Dcos(inclination )) * q;
        newLocalPositionZ = (MathUtilities.Dcos(argumentOfPeriapsis ) * MathUtilities.Dsin(longitudeOfAscendingNode ) + MathUtilities.Dsin(argumentOfPeriapsis ) * MathUtilities.Dcos(longitudeOfAscendingNode ) * MathUtilities.Dcos(inclination )) * p +
            (-MathUtilities.Dsin(argumentOfPeriapsis ) * MathUtilities.Dsin(longitudeOfAscendingNode ) + MathUtilities.Dcos(argumentOfPeriapsis ) * MathUtilities.Dcos(longitudeOfAscendingNode ) * MathUtilities.Dcos(inclination )) * q;
        newLocalPositionY = (MathUtilities.Dsin(argumentOfPeriapsis ) * MathUtilities.Dsin(inclination )) * p + (MathUtilities.Dcos(argumentOfPeriapsis ) * MathUtilities.Dsin(inclination )) * q;

        // create new local position
        Vector3 newLocalPosition = new Vector3((float)newLocalPositionX, (float)newLocalPositionY, (float)newLocalPositionZ);

        // return new position scaled to game units and in relation to the primary
        return newLocalPosition / M_PER_GU + primary.transform.position;
    }

    public Vector3 CalculateOrbitalPositionAtDate(double julianDate)
    {
        Ephemeris ephemerisAtDate = null;
        double centuriesSinceJ2000 = Engine.JulianDateToCenturiesSinceJ2000(julianDate);
        bool validEphemeris = CalculateEphemeris(ephemerisAtDate, centuriesSinceJ2000);

        if(validEphemeris)
            return CalculateOrbitalPosition(ephemerisAtDate.eccentricAnomaly);

        return Vector3.zero;
    }

    private void UpdateVelocity()
    {
        if(primary != null)
        {
            orbitalRadiusMeasured = Vector3.Distance(primary.transform.localPosition, transform.localPosition) * M_PER_GU;

            double my = G * primary.mass;
            double ean = eccentricAnomaly * Mathf.Deg2Rad;

            // generate orbital frame coordinate system
            double o = Math.Sqrt(my * semiMajorAxis) / orbitalRadiusMeasured;
            double ox = -Math.Sin(ean) * o;
            double oz = Math.Cos(ean) * Math.Sqrt(1 - Math.Pow(eccentricity, 2)) * o;

            double arg = argumentOfPeriapsis * Mathf.Deg2Rad;
            double lan = longitudeOfAscendingNode * Mathf.Deg2Rad;
            double inc = inclination * Mathf.Deg2Rad;

            // JPL method:
            double newLocalVelocityX = (Math.Cos(arg) * Math.Cos(lan) - Math.Sin(arg) * Math.Sin(lan) * Math.Cos(inc)) * ox -
                (Math.Sin(arg) * Math.Cos(lan) + Math.Cos(arg) * Math.Sin(lan) * Math.Cos(inc)) * oz;
            double newLocalVelocityZ = (Math.Cos(arg) * Math.Sin(lan) + Math.Sin(arg) * Math.Cos(lan) * Math.Cos(inc)) * ox +
                (Math.Cos(arg) * Math.Cos(lan) * Math.Cos(inc) - Math.Sin(arg) * Math.Sin(lan)) * oz;
            double newLocalVelocityY = (Math.Sin(arg) * Math.Sin(inc)) * ox + (Math.Cos(arg) * Math.Sin(inc)) * oz;

            // calculate initial movement direction
            velocity.Set((float)newLocalVelocityX, (float)newLocalVelocityY, (float)newLocalVelocityZ);
            velocity *= 1 / M_PER_GU;
        }
    }

    private void UpdateRotation(bool editorUpdate = false)
    {
        // rotation period
        if(siderialRotationPeriod != 0f && !Simulation.FreezeRotations)
        {
            // rotate around Y-axis based on the current real date/time UTC
            Quaternion tilt = Quaternion.Euler(axialTilt, 0f, 0f);
            transform.localRotation = tilt;

            // the following rotation settings are specific to Earth only and depend on the Coordinated Universal Time (UTC)
            // (absolute simulation time approach)
            if(name == "Earth")
            {
                DateTime now = Engine.DateInSimulation;

                rotationProgress = (now.Hour * Engine.SECONDS_PER_HOUR + now.Minute * Engine.SECONDS_PER_MINUTE + now.Second) / Engine.SECONDS_PER_DAY;
                rotationPeriod = now.Year * 365 + now.DayOfYear + rotationProgress;

                rotationAngle = MathUtilities.NormalizeAngle(270f - 360f * rotationProgress);
                noonLongitude = MathUtilities.NormalizeLongitude(-360f * rotationProgress + 180f);

                Quaternion sunDirection = new Quaternion();
                sunDirection.SetLookRotation(transform.position - primary.transform.position);

                Quaternion siderealRotation = Quaternion.Euler(0f, rotationAngle, 0f);

                //transform.localRotation *= sunDirection * siderealRotation;
                transform.localRotation *= sunDirection * siderealRotation;

                foreach(Transform cloudLayer in cloudLayers)
                {
                    if(cloudLayer != null)
                    {
                        float cloudLayerRotationProgress = (float)((rotationPeriod * (1f - cloudLayerSiderealRotationCoeffient)) % 1);
                        float cloudLayerRotationAngle = MathUtilities.NormalizeAngle(360f * cloudLayerRotationProgress);

                        Quaternion cloudLayerRotation = Quaternion.Euler(0f, cloudLayerRotationAngle, 0f);
                        cloudLayer.localRotation = tilt;
                        cloudLayer.localRotation *= sunDirection * cloudLayerRotation;
                    }
                }
            }

            // rotation for any other body (relative delta time approach)
            else
            {
                // rotate around Y-axis based on siderial rotation period
                float deltaRotationAngle = 360f * Time.deltaTime * Engine.TimeScale / (siderialRotationPeriod * S_PER_DAY);

                rotationAngle = MathUtilities.ClampAngle(rotationAngle + deltaRotationAngle);
                transform.localRotation *= Quaternion.Euler(0f, rotationAngle, 0f);
                rotationProgress = rotationAngle / 360f;
            }
        }

        if(terminator != null)
        {
            terminator.UpdatePlaneRotation();
        }
    }

    private void UpdateOrbitalEnergy()
    {
        if(primary != null)
        {
            orbitalEnergy = -(G * primary.mass) / 2 * semiMajorAxis;
        }
    }

    private void UpdateOrbitalPeriod()
    {
        if(primary != null)
        {
            orbitalPeriod = 2 * Math.PI * Math.Sqrt(Math.Pow(semiMajorAxis, 3) / (G * (primary.mass + this.mass)));
            orbitalPeriodRealtime = orbitalPeriod / Engine.TimeScale;
            orbitalPeriodString = Engine.TimeToString(orbitalPeriod);
            meanMotion = 360 / orbitalPeriod;
        }
    }

    private void UpdateAnomaly()
    {
        if(primary != null)
        {
            if(meanAnomaly == 360)
            {
                eccentricAnomaly = trueAnomaly = 360;
            }
            else
            {
                eccentricAnomaly = AstroUtilities.CalculateEccentricAnomaly(meanAnomaly, eccentricity);
                trueAnomaly = AstroUtilities.CalculateTrueAnomaly(eccentricAnomaly, eccentricity);
                if(trueAnomaly < 0)
                    trueAnomaly = 360 + trueAnomaly;
            }
        }
    }

    private void UpdateDates()
    {
        secondsSincePeriapsis = orbitalPeriod * orbitalCycleProgress;
        if(secondsSincePeriapsis > 0f)
        {
            timeOfPeriapsis = Engine.JulianDateInSimulation - secondsSincePeriapsis / Engine.SECONDS_PER_DAY;
            TimeSpan timeSincePeriapsis = TimeSpan.FromSeconds(secondsSincePeriapsis);
            dateOfPeriapsis = Engine.DateInSimulation.Subtract(timeSincePeriapsis);
        }
        else
        {
            timeOfPeriapsis = Engine.JulianDateInSimulation;
            dateOfPeriapsis = Engine.DateInSimulation;
        }
        dateOfPeriapsisString = dateOfPeriapsis.ToString("yyyy/MM/dd HH:mm");
    }

    private void UpdateInputs()
    {
        // the following switches are for the selected body only
        if(ReferenceEquals(this, Simulation.SelectedBody))
        {
            if(Input.GetKeyDown(KeyCode.L))
            {
                //if(Input.GetKey(KeyCode.LeftShift) && renderLabel == 0)
                //    renderLabel = SWITCH_PREPARE_OFFSET + 1;
                //else if(Input.GetKey(KeyCode.LeftShift) && renderLabel == 1)
                //    renderLabel = SWITCH_PREPARE_OFFSET;

                renderLabel = ToggleSwitch(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift), renderLabel);
            }

            else if(Input.GetKeyDown(KeyCode.T))
            {
                //if(Input.GetKey(KeyCode.LeftShift) && renderTrail == 0)
                //    renderTrail = SWITCH_PREPARE_OFFSET + 1;
                //else if(Input.GetKey(KeyCode.LeftShift) && renderTrail == 1)
                //    renderTrail = SWITCH_PREPARE_OFFSET;

                renderTrail = ToggleSwitch(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift), renderTrail);
            }

            //else if(Input.GetKeyDown(KeyCode.I))
            //{
            //    Engine.DebugLog("Body.UpdateInputs(): render label = {0}", renderLabel);
            //}
        }
    }

    private void UpdateLabels()
    {
        if(ReferenceEquals(this, Simulation.SelectedBody) && distancesInfoLabel != null && distancesNamesLabel != null)
        {
            // get other bodies in system
            Transform solarSystem = GameObject.Find("Universe/Solar System").transform;
            List<Body> bodies = new List<Body>();
            solarSystem.GetComponentsInChildren<Body>(false, bodies);

            bodies.Remove(this);
            bodies.Sort((body1, body2) => (int)(body1.semiMajorAxis - body2.semiMajorAxis));

            string distancesNames = string.Format("<color=white>DISTANCES FROM {0}</color>\n", name.ToUpper());
            string distancesInfo = "\n";

            foreach(Body body in bodies)
            {
                float distanceAu = Vector3.Distance(transform.position, body.transform.position) * AU_PER_GU;
                distancesNames += body.name.ToUpper() + '\n';
                distancesInfo += string.Format("{0:0.000} AU\n", distanceAu);
            }

            distancesNamesLabel.text = distancesNames;
            distancesInfoLabel.text = distancesInfo;
        }
    }

    private int ToggleSwitch(bool condition, int value)
    {
        if(condition && value == 0)
            return PrepareSwitch(1);
        else if(condition && value == 1)
            return PrepareSwitch(0);
        return value;
    }

    private int PrepareSwitch(int value)
    {
        return SWITCH_PREPARE_OFFSET + value;
    }

    private void InitializeOrbitalMeasurements()
    {
        semiMajorAxisMeasured = semiMajorAxis;
        periapsisMeasured = periapsis;
        apoapsisMeasured = apoapsis;
        eccentricityMeasured = eccentricity;
        orbitalPeriodMeasured = orbitalPeriod;
        orbitalPeriodMeasuredString = orbitalPeriodString;
        inclinationMeasured = inclination;
    }

    // measures and calculates secondary orbital characteristics
    private void UpdateOrbitalMeasurements()
    {
        if(primary == null)
            return;

        /* cartesian orbital state vectors to orbital elements calculation */

        orbitalRadiusMeasured = Vector3.Distance(primary.transform.localPosition, transform.localPosition) * M_PER_GU;

        // position, velocity and momentum are updated in Actuate()
        float truePositionMagnitude = truePosition.magnitude;
        float trueVelocityMagnitude = trueVelocity.magnitude;
        float momentumMagnitude = momentum.magnitude;

        eccentricityVector = Vector3.Cross(trueVelocity, momentum) / (G * primary.mass) - truePosition.normalized;
        eccentricityMeasured = eccentricityVector.magnitude;
        inclinationMeasured = 180 - Mathf.Rad2Deg * Math.Acos(momentum.y / momentumMagnitude);

        n = Vector3.Cross(Vector3.up, momentum);
        float nMagnitude = n.magnitude;

        longitudeOfAscendingNodeMeasured = Math.Acos(n.x / nMagnitude);
        if(n.z < 0)
            longitudeOfAscendingNodeMeasured = 2 * Math.PI - longitudeOfAscendingNodeMeasured;
        longitudeOfAscendingNodeMeasured *= Mathf.Rad2Deg;

        argumentOfPeriapsisMeasured = Math.Acos(Vector3.Dot(n, eccentricityVector) / (nMagnitude * eccentricityMeasured));
        if(eccentricityVector.y < 0)
            argumentOfPeriapsisMeasured = 2 * Math.PI - argumentOfPeriapsisMeasured;
        argumentOfPeriapsisMeasured *= Mathf.Rad2Deg;

        if(eccentricityMeasured > 0)
        {
            trueAnomalyMeasured = Math.Acos(Vector3.Dot(eccentricityVector, truePosition) / (eccentricityMeasured * truePositionMagnitude));
            if(Vector3.Dot(truePosition, velocity) < 0)
                trueAnomalyMeasured = 2 * Math.PI - trueAnomalyMeasured;

            eccentricAnomalyMeasured = 2 * Math.Atan(Math.Tan(trueAnomalyMeasured / 2) / Math.Sqrt((1 + eccentricityMeasured) / (1 - eccentricityMeasured)));
            meanAnomalyMeasured = eccentricAnomalyMeasured - eccentricityMeasured * Math.Sin(eccentricAnomalyMeasured);

            if(eccentricAnomalyMeasured < 0f)
                eccentricAnomalyMeasured = 2 * Mathf.PI + eccentricAnomalyMeasured;

            if(meanAnomalyMeasured < 0f)
                meanAnomalyMeasured = 2 * Mathf.PI + meanAnomalyMeasured;

            // rad to degree conversion
            trueAnomalyMeasured *= Mathf.Rad2Deg;
            eccentricAnomalyMeasured *= Mathf.Rad2Deg;
            meanAnomalyMeasured *= Mathf.Rad2Deg;
        }
        else
        {
            trueAnomalyMeasured = eccentricAnomalyMeasured = meanAnomalyMeasured = 0f;
        }

        semiMajorAxisMeasured = 1 / (2 / truePositionMagnitude - trueVelocityMagnitude * trueVelocityMagnitude / (G * primary.mass));
        periapsisMeasured = semiMajorAxisMeasured * (1 - eccentricityMeasured);
        apoapsisMeasured = semiMajorAxisMeasured * (1 + eccentricityMeasured);

        orbitalEnergyMeasured = -(G * primary.mass) / 2 * semiMajorAxisMeasured;
        orbitalPeriodMeasured = 2 * Math.PI * Math.Sqrt(Math.Pow(semiMajorAxisMeasured, 3) / (G * (primary.mass + this.mass)));
        orbitalPeriodRealtimeMeasured = orbitalPeriodMeasured / Engine.TimeScale;
        orbitalPeriodMeasuredString = Engine.TimeToString(orbitalPeriodMeasured);
        orbitalCycleProgress = meanAnomalyMeasured / 360f;

        meanMotionMeasured = 360f / orbitalPeriodMeasured;

        /* count orbital cycles */

        timeSinceLastCompletedOrbitalCycle += Time.deltaTime * Engine.TimeScale;
        if(timeSinceLastCompletedOrbitalCycle >= orbitalPeriodMeasured)
        {
            orbitalCyclesCompleted++;
            timeSinceLastCompletedOrbitalCycle = 0f;
        }

        /* added physical measurements */

        axialTiltMeasured = Mathf.Acos(Vector3.Dot(GetOrbitalPlaneNormal().normalized, transform.up)) * Mathf.Rad2Deg;
    }

    public void ClearTrail()
    {
        // set mark to clear trails after the next position update
        clearTrailsOnNextPositionUpdate = 2;
    }

    void OnMouseDown()
    {
        if((Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftAlt)) || (Input.GetKey(KeyCode.RightControl) && Input.GetKey(KeyCode.RightAlt)))
        {
            Simulation.ReferencedBody = this;
            MayaCamera.SetPointOfViewCameraMode(this);
        }
        else if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            Simulation.SelectedBody = this;
            MayaCamera.target = transform;
        }
        else
        {
            // only set this body as the follow target if the current camera isn't a child of it
            if(!Target.CurrentCamera.transform.IsChildOf(transform))
            {
                Target.FollowTarget = transform;
                Target.FollowMode = CameraFollowMode.followSystemBody;
                Engine.DebugLog("set follow target to {0}", NameRichtext);
            }
        }
    }

    void OnCollisionEnter(Collision collider)
    {
        //Body body = collider.gameObject.GetComponent<Body>();
        //if(body != null && body.mass < this.mass)
        //{
        //    Destroy(collider.gameObject);

        //    Engine.DebugLog("{0} collided with {1}.", NameRichtext, body.NameRichtext);
        //}
    }

    void OnDestroy()
    {
        if(system != null)
        {
            system.UnregisterSatellite(this);
        }

        //Engine.DebugLog("{0} has been destroyed.", NameRichtext);
    }

    void OnBecameVisible()
    {
        if(renderLabel == RENDER_LABEL_INVISIBLE)
            renderLabel = SWITCH_PREPARE_OFFSET + RENDER_LABEL_ON;
    }

    void OnBecameInvisible()
    {
        if(renderLabel == RENDER_LABEL_ON)
            renderLabel = SWITCH_PREPARE_OFFSET + RENDER_LABEL_INVISIBLE;
    }

    // Calculate force between this and another body
    public Vector3 ForceBetween(Body other)
    {
        mutualForceVector = other.transform.localPosition - transform.localPosition;

        float distance = mutualForceVector.magnitude * M_PER_GU;

        // to avoid almost infinite acceleration forces due to almost zero distances (only in case of disabled collisions)
        if(distance < 0.5f * M_PER_GU)
            distance = 0.5f * M_PER_GU;

        float force = (G * mass * other.mass) / (distance * distance);

        mutualForceVector = mutualForceVector.normalized * force;

        return mutualForceVector;
    }

    // Actuates and moves this body in respect to a given force
    public void Actuate(Vector3 force, float deltaTime)
    {
        attraction = force;
        acceleration = force / mass / M_PER_GU;

        // use simple Euler integration
        velocity += acceleration * deltaTime * Engine.TimeScale;
        transform.localPosition += velocity * deltaTime * Engine.TimeScale;

        //if(clearTrailsOnNextPositionUpdate && trail != null)
        //{
        //    trail.Clear();
        //    clearTrailsOnNextPositionUpdate = false;
        //}

        orbitalSpeed = velocity.magnitude * M_PER_GU;
        orbitalSpeedKMS = velocity.magnitude * KM_PER_GU;

        trueVelocity = velocity * M_PER_GU;
        truePosition = transform.localPosition * M_PER_GU;
        momentum = Vector3.Cross(truePosition, trueVelocity);
    }

    public Vector3 GetOrbitalPlaneNormal()
    {
        // if the body has no velocity return the transform's up vector (i.e. let orbital and equatorial plane be the same)
        if(velocity.magnitude == 0f)
            return transform.up;
        // otherwise return the orbital plane as the cross product of velocity and the position relative to the primary
        else if(primary != null)
        {
            Vector3 positionRelativeToPrimary = transform.position - primary.transform.position;
            return Vector3.Cross(velocity, positionRelativeToPrimary);
            //return Vector3.Cross(velocity, transform.localPosition);
        }
        else
            return Vector3.zero;
    }

    public static float ConvertAUtoKM(float AU)
    {
        return AU * KM_PER_AU;
    }
}
